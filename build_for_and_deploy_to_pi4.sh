## pi4
export target=aarch64-unknown-linux-gnu
export features=battery,sysinfo,thermal_zones,http_server,fronius,external,influxdb_relay,track_source,signal_notifier,worx,alias,alias_regex,subscription_sender,signal_watch_debugging
#,signal_receive
#,signal_cli_debugging
# 2024-07-18 08:30 with panic-info: 2062960 -> 597072 = 2mb -> 597kb
# 2024-07-18 08:40   no debug-flag: 1366376 -> 417280 = 1.37mb -> 417kb
# 2024-08-14 16:45: 6247480 -> 2375108 = 6.25mb -> 2.38mb (worx https client requires ssl lib - need to use embedded version because cross image is super old with ssl v1.0 which is incompatible with todays linux distros!)
#,external_debugging

## build with debugging info telling which src file and line caused a panic:
export debug=1

bash build_smaller_release.sh

ssh pi4 -t 'cd /opt/; mv --verbose --no-clobber rust-influxdb-udp-logger "rust-influxdb-udp-logger_$(date -r rust-influxdb-udp-logger +%F_%H%M)"'
scp target/aarch64-unknown-linux-gnu/release/rust-influxdb-udp-logger pi4:/opt/
ssh pi4 -t 'systemctl restart pi4_temperature_logger.service & killall -KILL signal-cli; journalctl -f -u pi4_temperature_logger.service'

## only look at log of currently running version:
#ssh pi4 -t 'journalctl -e -u pi4_temperature_logger.service'
