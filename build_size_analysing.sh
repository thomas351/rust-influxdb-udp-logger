cargo install cargo-bloat

# http (reqwest + ssl) moved behind feature flag & disabled:
cargo bloat --release --crates
: '
 File  .text     Size Crate
 8.2%  30.9% 388.2KiB std
 4.7%  17.5% 220.2KiB regex_automata
 2.9%  11.1% 139.1KiB clap_builder
 2.8%  10.5% 132.2KiB rust_influxdb_udp_logger
 2.1%   7.8%  97.7KiB aho_corasick
 2.0%   7.4%  93.2KiB regex_syntax
 1.4%   5.2%  65.7KiB tokio
 0.7%   2.7%  33.3KiB chrono
 0.3%   1.3%  15.8KiB serde_json
 0.3%   1.2%  15.4KiB regex
 0.2%   0.9%  10.7KiB memchr
 0.2%   0.6%   8.0KiB tokio_modbus
 0.2%   0.6%   7.9KiB starship_battery
 0.1%   0.3%   3.6KiB sysinfo
 0.1%   0.3%   3.2KiB bytes
 0.0%   0.2%   2.0KiB num_cpus
 0.0%   0.2%   1.9KiB [Unknown]
 0.0%   0.1%   1.6KiB anstream
 0.0%   0.1%   1.5KiB anstyle
 0.0%   0.1%   1.5KiB clap_lex
 0.2%   0.6%   7.3KiB And 15 more crates. Use -n N to show more.
26.5% 100.0%   1.2MiB .text section size, the file size is 4.6MiB
'

cargo bloat --release 
: '
 File  .text     Size                     Crate Name
 1.5%   5.6%  70.6KiB            regex_automata regex_automata::meta::strategy::new
 0.5%   2.1%  25.7KiB  rust_influxdb_udp_logger rust_influxdb_udp_logger::main::{{closure}}
 0.5%   1.7%  21.3KiB              clap_builder clap_builder::parser::parser::Parser::get_matches_with
 0.4%   1.4%  17.6KiB  rust_influxdb_udp_logger rust_influxdb_udp_logger::fronius_read::{{closure}}
 0.3%   1.3%  16.2KiB                       std std::backtrace_rs::symbolize::gimli::Context::new
 0.3%   1.2%  15.4KiB                     regex regex::builders::string::RegexBuilder::build
 0.3%   1.2%  15.0KiB  rust_influxdb_udp_logger rust_influxdb_udp_logger::sysinfo::{{closure}}
 0.3%   1.2%  14.6KiB  rust_influxdb_udp_logger rust_influxdb_udp_logger::fronius_watcher::{{closure}}
 0.3%   1.2%  14.6KiB                       std std::sys_common::backtrace::_print_fmt::{{closure}}
 0.2%   0.9%  11.4KiB  rust_influxdb_udp_logger rust_influxdb_udp_logger::watch_batteries::{{closure}}
 0.2%   0.9%  10.9KiB                       std addr2line::ResUnit<R>::find_function_or_location::{{closure}}
 0.2%   0.9%  10.8KiB                       std std::process::Command::output
 0.2%   0.9%  10.7KiB                     tokio tokio::runtime::builder::Builder::build
 0.2%   0.8%  10.0KiB              aho_corasick aho_corasick::packed::api::Builder::build
 0.2%   0.8%   9.9KiB rust_influxdb_udp_logger? <rust_influxdb_udp_logger::Cli as clap_builder::derive::CommandFactory>::command
 0.2%   0.8%   9.8KiB                       std gimli::read::dwarf::Unit<R>::new
 0.2%   0.7%   9.3KiB            regex_automata regex_automata::nfa::thompson::compiler::Compiler::c
 0.2%   0.7%   9.1KiB                       std miniz_oxide::inflate::core::decompress
 0.2%   0.7%   8.9KiB                       std addr2line::Lines::parse
 0.2%   0.7%   8.7KiB  rust_influxdb_udp_logger rust_influxdb_udp_logger::main
19.6%  74.0% 929.3KiB                           And 4003 smaller methods. Use -n N to show more.
26.5% 100.0%   1.2MiB                           .text section size, the file size is 4.6MiB
'

# now also external command processing = regex moved behind feature flag:
: '
 File  .text     Size Crate
11.2%  44.3% 344.2KiB std
 4.9%  19.3% 149.8KiB clap_builder
 4.0%  15.8% 122.6KiB rust_influxdb_udp_logger
 2.2%   8.5%  66.2KiB tokio
 1.1%   4.3%  33.2KiB chrono
 0.5%   2.0%  15.8KiB serde_json
 0.3%   1.2%   9.7KiB tokio_modbus
 0.3%   1.0%   7.9KiB starship_battery
 0.1%   0.5%   4.2KiB sysinfo
 0.1%   0.4%   3.2KiB bytes
 0.1%   0.2%   1.7KiB [Unknown]
 0.1%   0.2%   1.7KiB anstream
 0.1%   0.2%   1.7KiB num_cpus
 0.0%   0.2%   1.5KiB anstyle
 0.0%   0.2%   1.4KiB clap_lex
 0.0%   0.2%   1.4KiB binread
 0.0%   0.2%   1.3KiB strsim
 0.0%   0.2%   1.3KiB iana_time_zone
 0.0%   0.1%     614B tokio_util
 0.0%   0.1%     607B ryu
 0.1%   0.3%   2.6KiB And 12 more crates. Use -n N to show more.
25.3% 100.0% 777.2KiB .text section size, the file size is 3.0MiB
'

# next we moved fronius loader = modbus & binread crates behind feature flag:
: '
File  .text     Size Crate
11.5%  47.3% 336.8KiB std
 5.1%  20.9% 148.9KiB clap_builder
 3.3%  13.6%  97.1KiB rust_influxdb_udp_logger
 2.1%   8.8%  62.8KiB tokio
 0.7%   2.9%  20.9KiB chrono
 0.5%   2.2%  15.8KiB serde_json
 0.3%   1.1%   7.9KiB starship_battery
 0.1%   0.6%   4.2KiB sysinfo
 0.1%   0.2%   1.7KiB [Unknown]
 0.1%   0.2%   1.7KiB anstream
 0.1%   0.2%   1.7KiB num_cpus
 0.1%   0.2%   1.5KiB anstyle
 0.0%   0.2%   1.4KiB clap_lex
 0.0%   0.2%   1.3KiB strsim
 0.0%   0.2%   1.3KiB iana_time_zone
 0.0%   0.1%     607B ryu
 0.0%   0.1%     572B futures_util
 0.0%   0.1%     488B mio
 0.0%   0.0%     319B socket2
 0.0%   0.0%     278B futures_io
 0.0%   0.1%     774B And 7 more crates. Use -n N to show more.
24.3% 100.0% 712.7KiB .text section size, the file size is 2.9MiB
'

# this time we moved valetudo json interface = serde_json crate behind disabled feature flag:
: '
 File  .text     Size Crate
11.8%  49.5% 334.1KiB std
 5.2%  21.9% 147.8KiB clap_builder
 3.0%  12.7%  85.9KiB rust_influxdb_udp_logger
 2.0%   8.5%  57.1KiB tokio
 0.7%   3.1%  21.0KiB chrono
 0.3%   1.2%   7.9KiB starship_battery
 0.1%   0.6%   4.2KiB sysinfo
 0.1%   0.3%   1.7KiB [Unknown]
 0.1%   0.3%   1.7KiB num_cpus
 0.1%   0.2%   1.6KiB anstream
 0.1%   0.2%   1.5KiB anstyle
 0.0%   0.2%   1.4KiB clap_lex
 0.0%   0.2%   1.3KiB strsim
 0.0%   0.2%   1.3KiB iana_time_zone
 0.0%   0.1%     572B futures_util
 0.0%   0.1%     488B mio
 0.0%   0.0%     319B socket2
 0.0%   0.0%     278B futures_io
 0.0%   0.0%     200B crossbeam_utils
 0.0%   0.0%     195B rayon
 0.0%   0.1%     390B And 5 more crates. Use -n N to show more.
23.8% 100.0% 675.6KiB .text section size, the file size is 2.8MiB
'

# without http & external & fronius & valetudo & battery & sysload
: '
 File  .text     Size Crate
11.8%  51.9% 317.9KiB std
 5.5%  24.0% 147.2KiB clap_builder
 2.0%   9.0%  55.1KiB rust_influxdb_udp_logger
 2.0%   8.8%  54.2KiB tokio
 0.8%   3.5%  21.5KiB chrono
 0.1%   0.3%   1.8KiB num_cpus
 0.1%   0.3%   1.7KiB anstream
 0.1%   0.3%   1.7KiB [Unknown]
 0.1%   0.2%   1.5KiB anstyle
 0.1%   0.2%   1.4KiB clap_lex
 0.0%   0.2%   1.3KiB strsim
 0.0%   0.2%   1.3KiB iana_time_zone
 0.0%   0.1%     794B futures_util
 0.0%   0.1%     488B mio
 0.0%   0.1%     319B socket2
 0.0%   0.0%     278B futures_io
 0.0%   0.0%      77B futures_executor
 0.0%   0.0%      74B futures_task
 0.0%   0.0%      39B anstyle_parse
22.7% 100.0% 612.9KiB .text section size, the file size is 2.6MiB
'