# Cross compile research 2024-01-28

I would like to build a rust program for this system:

```log
$ uname -a
Linux rockrobo 3.4.39 #1 SMP PREEMPT Fri Dec 23 12:20:10 CST 2022 armv7l GNU/Linux
```

How can I know which one of those two targets I need?

- armv7-unknown-linux-gnueabihf
- armv7-unknown-linux-musleabihf

I'm pretty sure it won't be `armv7-linux-androideabi` since it's not android, but I don't know how to find if I need gnueabihf or musleabihf. Also is the little-endian byte ordering need to be taken into account when compiling (the l behind armv7)?

UPDATE: okey, after a bit of googling I found how to find the correct libc for my target system:

```log
[root@rockrobo ~]# ldd /bin/bash
        libtinfo.so.5 => /lib/arm-linux-gnueabihf/libtinfo.so.5 (0xb6f9f000)
        libdl.so.2 => /lib/arm-linux-gnueabihf/libdl.so.2 (0xb6f94000)
        libc.so.6 => /lib/arm-linux-gnueabihf/libc.so.6 (0xb6ead000)
        /lib/ld-linux-armhf.so.3 (0xb6fc4000)
```

So it's gnueabihf.

UPDATE2: I've managed to solve my first compiler error `failed to run custom build command for ``openssl-sys v0.9.91\`` (it seems some dependency uses openssl) by following [this recommendation](https://github.com/cross-rs/cross/wiki/Recipes):

```toml
openssl = { version = "0.10", features = ["vendored"] }
```

With that I managed to build a 122mb debug build which is much too big for my system, so I added the `--release`  build option, which to me surprisingly made it fail to build again, with the error that none of the 3 glibc versions 2.23, 2.33 or 2.34 could be found?

Is this caused by my host system having libc version 2.38, or is this an incompatibility between the container used by cross and the libraries used by my rust program?

UPDATE3: I found this: <https://github.com/cross-rs/cross/discussions/1025> which said the solution for the problem is to `cargo clean` = delete the target directory and start a fresh build. That worked and got me a 4mb big release build I could try on my target system.

Sadly, it doesn't work, because my target system features:

```log
[root@rockrobo data]# ldd --version
ldd (Ubuntu EGLIBC 2.19-0ubuntu6.6) 2.19
```

-> it's `Ubuntu 14.04.3 LTS` aparently..

And the compiled binary expects one of 2.25, 2.28 or 2.29...

How can I fix this? I guess there's two paths forward: either get a build environment (=docker container?) with a compatible libc version, or build with a statically linked (=included) libc so it doesn't care for the libc version of the target system. Correct? Which one do you think would be easier?

UPDATE4: I've found this [pull request for CentOS7](https://github.com/cross-rs/cross/pull/1006) which seems to solve a similar problem.

UPDATE5: Ah, I've now found that the `armv7-unknown-linux-musleabihf` target does statically link = include the musl libc, so it does run fine on my target even though the libc installed on that system is gnu libc 2.19 :)
