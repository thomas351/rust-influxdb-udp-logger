use crate::{log, log_error, Measurement, MeasurementSource};
use rumqttd::{local::LinkRx, Broker, Config, Notification};
use std::time::{SystemTime, UNIX_EPOCH};
use tokio::{sync::mpsc::Sender, task::JoinHandle};

pub fn setup(tx: Sender<Measurement>) -> Vec<JoinHandle<()>> {
    let mut handles: Vec<JoinHandle<()>> = vec![];

    // see docs of config crate to know more
    let config = config::Config::builder()
        .add_source(config::File::with_name("rumqttd.toml"))
        .build()
        .unwrap();

    // this is where we deserialize it into Config
    let rumqttd_config: Config = config.try_deserialize().unwrap();
    let mut broker = Broker::new(rumqttd_config);
    let (mut link_tx, link_rx) = broker.link("singlenode").unwrap();
    link_tx.subscribe("#").unwrap();

    handles.push(tokio::spawn(async move {
        broker
            .start()
            .map_err(|e| log_error(&format!("Mqtt relay broker failed! {e}")))
            .ok();
    }));
    handles.push(tokio::spawn(listener(link_rx, tx)));
    handles
}

async fn listener(mut link_rx: LinkRx, tx: Sender<Measurement>) {
    loop {
        let notification = match link_rx.recv() {
            Ok(Some(v)) => v,
            Ok(None) => {
                log_error("MQTT listener received None");
                continue;
            }
            Err(e) => {
                log_error(&format!("MQTT listener received error: {e}"));
                continue;
            }
        };
        match notification {
            Notification::Forward(forward) => {
                #[cfg(feature = "mqtt_relay_debugging")]
                log(&format!("MQTT: {forward:?}"));
                let prefix_cow = String::from_utf8_lossy(&forward.publish.topic);
                let value_str = String::from_utf8_lossy(&forward.publish.payload);
                if value_str.is_empty() {
                    log(&format!("Mqtt relay: empty data for topic: {prefix_cow}"));
                    continue;
                }
                let value = match value_str.parse() {
                    Ok(v) => v,
                    Err(_) => {
                        log(&format!(
                            "Mqtt relay: non-numeric data: {prefix_cow}={value_str}"
                        ));
                        continue;
                    }
                };
                let prefix: String = " MQTT.".to_owned() + prefix_cow.as_ref(); // + &prefix_cow.replace(':', "");
                let time = SystemTime::now()
                    .duration_since(UNIX_EPOCH)
                    .unwrap()
                    .as_nanos();
                // [can we see the source ip & port of a topic update?](https://github.com/bytebeamio/rumqtt/issues/891)
                tx.send(Measurement::new(
                    prefix,
                    value,
                    time,
                    #[cfg(feature = "track_source")]
                    MeasurementSource::MqttRelay,
                ))
                .await
                .ok();
            }
            v => {
                log(&format!("MQTT non-forward Notification: {v:?}"));
            }
        }
    }
}
