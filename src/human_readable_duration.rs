use core::fmt;
use std::time::Duration;

pub struct HumanReadableDuration {
    weeks: u32,
    days: u8,
    hours: u8,
    minutes: u8,
    seconds: u8,
    milliseconds: u32,
}

impl fmt::Display for HumanReadableDuration {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match (
            self.weeks,
            self.days,
            self.hours,
            self.minutes,
            self.seconds,
            self.milliseconds,
        ) {
            (0, 0, 0, 0, s, 0) => {
                write!(f, "{s}s")
            }
            (0, 0, 0, 0, s, ms) => {
                write!(f, "{s}.{ms:03}s")
            }
            (0, 0, 0, m, s, 0) => {
                write!(f, "{m}m {s}s")
            }
            (0, 0, 0, m, s, ms) => {
                write!(f, "{m}m {s}.{}s", ms / 100)
            }
            (0, 0, h, m, s, _) => {
                write!(f, "{h}h {m}m {s}s")
            }
            (0, d, h, m, _, _) => {
                write!(f, "{d}d {h}h {m}m")
            }
            (w, d, h, _, _, _) => {
                write!(f, "{w}w {d}d {h}h")
            }
        }
    }
}

pub fn format_duration(duration: Duration) -> HumanReadableDuration {
    let secs = duration.as_secs();
    let (mins, secs) = (secs / 60, secs % 60);
    let (hours, mins) = (mins / 60, mins % 60);
    let (days, hours) = (hours / 24, hours % 24);
    let (weeks, days) = (days / 7, days % 7);

    HumanReadableDuration {
        weeks: weeks as u32,
        days: days as u8,
        hours: hours as u8,
        minutes: mins as u8,
        seconds: secs as u8,
        milliseconds: duration.subsec_millis(),
    }
}
