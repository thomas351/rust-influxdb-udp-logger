use starship_battery::{Battery, Manager};
use std::{
    borrow::Cow::{Borrowed, Owned},
    time::{Duration, SystemTime, UNIX_EPOCH},
};
use tokio::sync::mpsc::Sender;
use tokio::time::sleep;

use crate::Measurement;

pub async fn watcher(hostname: String, query_frequency: Duration, tx: Sender<Measurement>) {
    let Ok(manager) = Manager::new() else {
        return;
    };
    let mut good_bats: Vec<Battery> = Vec::new();
    let Ok(batteries) = manager.batteries() else {
        return;
    };
    for battery in batteries.into_iter().flatten() {
        good_bats.push(battery);
    }

    if !good_bats.is_empty() {
        let multi = good_bats.len() > 1;
        loop {
            for battery in &mut good_bats {
                let time = SystemTime::now()
                    .duration_since(UNIX_EPOCH)
                    .unwrap()
                    .as_nanos();
                manager.refresh(battery).ok();
                let battery_name = if multi {
                    Owned(
                        format!(
                            "battery_{}_{}",
                            battery.model().unwrap_or(""),
                            battery.serial_number().unwrap_or("")
                        )
                        .replace(' ', "_")
                        .replace("__", "_"),
                    )
                } else {
                    Borrowed("battery")
                };
                if let Some(count) = battery.cycle_count() {
                    let prefix = format!(" {hostname}.{battery_name}_cycles");
                    let m = Measurement::new(
                        prefix,
                        count as _,
                        time,
                        #[cfg(feature = "track_source")]
                        crate::MeasurementSource::Battery,
                    );
                    tx.send(m).await.ok();
                }
                if let Some(temperature) = battery.temperature() {
                    let prefix = format!(",zone={battery_name} {hostname}.temperature");
                    let m = Measurement::new(
                        prefix,
                        temperature.value as _,
                        time,
                        #[cfg(feature = "track_source")]
                        crate::MeasurementSource::Battery,
                    );
                    tx.send(m).await.ok();
                }
                if let Some(time_to_empty) = battery.time_to_empty() {
                    let prefix = format!(" {hostname}.{battery_name}_time_empty");
                    let m = Measurement::new(
                        prefix,
                        time_to_empty.value as _,
                        time,
                        #[cfg(feature = "track_source")]
                        crate::MeasurementSource::Battery,
                    );
                    tx.send(m).await.ok();
                }
                if let Some(time_to_full) = battery.time_to_full() {
                    let prefix = format!(" {hostname}.{battery_name}_time_full");
                    let m = Measurement::new(
                        prefix,
                        time_to_full.value as _,
                        time,
                        #[cfg(feature = "track_source")]
                        crate::MeasurementSource::Battery,
                    );
                    tx.send(m).await.ok();
                }
                let prefix = format!(" {hostname}.{battery_name}_state");
                let m = Measurement::new(
                    prefix,
                    (battery.state() as u8) as _,
                    time,
                    #[cfg(feature = "track_source")]
                    crate::MeasurementSource::Battery,
                );
                tx.send(m).await.ok();

                // skip redundant info: charge = energy/energy-full & health = energy-full / energy-design
                // let id = prefix.clone() + "_charge";
                // send_sysvar(&mut last_sent, &id, battery.state_of_charge().value as f64, current_time, minimum_frequency, format!(" {hostname}.{id}"), &tx).await;
                // let id = prefix.clone() + "_health";
                // send_sysvar(&mut last_sent, &id, battery.state_of_health().value as f64, current_time, minimum_frequency, format!(" {hostname}.{id}"), &tx).await;

                let prefix = format!(" {hostname}.{battery_name}_energy_now");
                let m = Measurement::new(
                    prefix,
                    battery.energy().value as _,
                    time,
                    #[cfg(feature = "track_source")]
                    crate::MeasurementSource::Battery,
                );
                tx.send(m).await.ok();

                let prefix = format!(" {hostname}.{battery_name}_energy_rate");
                let m = Measurement::new(
                    prefix,
                    battery.energy_rate().value as _,
                    time,
                    #[cfg(feature = "track_source")]
                    crate::MeasurementSource::Battery,
                );
                tx.send(m).await.ok();

                let prefix = format!(" {hostname}.{battery_name}_energy_full");
                let m = Measurement::new(
                    prefix,
                    battery.energy_full().value as _,
                    time,
                    #[cfg(feature = "track_source")]
                    crate::MeasurementSource::Battery,
                );
                tx.send(m).await.ok();

                let prefix = format!(" {hostname}.{battery_name}_energy_design");
                let m = Measurement::new(
                    prefix,
                    battery.energy_full_design().value as _,
                    time,
                    #[cfg(feature = "track_source")]
                    crate::MeasurementSource::Battery,
                );
                tx.send(m).await.ok();

                let prefix = format!(" {hostname}.{battery_name}_voltage");
                let m = Measurement::new(
                    prefix,
                    battery.voltage().value as _,
                    time,
                    #[cfg(feature = "track_source")]
                    crate::MeasurementSource::Battery,
                );
                tx.send(m).await.ok();

                #[cfg(feature = "battery_debugging")]
                println!("{:?}", battery);
            }
            sleep(query_frequency).await;
        }
    }
}
