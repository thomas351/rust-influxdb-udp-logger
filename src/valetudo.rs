use serde_json::{from_str, to_string_pretty, Value as JsonValue};
use std::{
    borrow::Cow::{self, Borrowed, Owned},
    net::SocketAddr,
    time::{Duration, SystemTime, UNIX_EPOCH},
};
use tokio::time::{sleep, timeout, Instant};
use tokio::{
    io::{AsyncBufReadExt, AsyncWriteExt},
    net::TcpStream,
    sync::mpsc::Sender,
};

use crate::{log, log_error, tcp_connect, Measurement};

async fn valetudo_sse_subscribe(addr: &SocketAddr) -> tokio::io::BufReader<TcpStream> {
    let mut stream = tcp_connect(addr).await;
    stream
        .write_all("GET /api/v2/robot/state/attributes/sse\r\n\r\n".as_bytes())
        .await
        .ok();
    tokio::io::BufReader::new(stream)
}

async fn valetudo_sender(data: &str, hostname: &str, tx: &Sender<Measurement>) {
    if let Ok(value) = from_str::<JsonValue>(data) {
        #[cfg(feature = "valetudo_debugging")]
        println!("parsed data: {}", to_string_pretty(&value).unwrap());
        let current_time = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_nanos();
        let mut status: Cow<str> = Borrowed("undefined");
        let mut battery_state: &str = "undefined";
        let mut battery_level: f64 = -1.0;
        // let mut mop = false;
        // let mut watertank = false;
        for element in value.as_array().unwrap() {
            match element["__class"].as_str() {
                Some("BatteryStateAttribute") => {
                    battery_level = element["level"].as_f64().unwrap();
                    battery_state = element["flag"].as_str().unwrap();
                }
                Some("StatusStateAttribute") => {
                    status = match element["value"].as_str() {
                        Some("error") => Owned(
                            ("error: ".to_owned() + element["error"]["message"].as_str().unwrap())
                                .replace(' ', "\\ "),
                        ),
                        Some(value) => Borrowed(value),
                        _ => Borrowed("parse_error"),
                    };
                }
                // Some("AttachmentStateAttribute") => {
                //     match element["type"].as_str() {
                //         Some("mop") => {mop = element["attached"].as_bool().unwrap_or(false);},
                //         Some("watertank") => {watertank = element["attached"].as_bool().unwrap_or(false);},
                //         _ => (),
                //     }
                // },
                _ => (),
            }
        }
        let prefix = format!(",status={status},state={battery_state} {hostname}.battery");
        let m = Measurement::auto_resend(
            prefix,
            battery_level,
            current_time,
            #[cfg(feature = "track_source")]
            crate::MeasurementSource::Valetudo,
        );
        tx.send(m).await.ok();
    } else {
        println!("couldn't parse data: {data}");
    }
}

async fn swagger_loader(addr: &SocketAddr, uri: &str) -> String {
    #[cfg(feature = "valetudo_debugging")]
    log(&format!("swagger_loader - started loading {addr}{uri}"));
    let stream = tcp_connect(addr).await;
    #[cfg(feature = "valetudo_debugging")]
    log("swagger_loader - opened TcpStream");
    let mut stream = tokio::io::BufReader::new(stream);
    stream
        .write_all(format!("GET {uri}\r\n\r\n").as_bytes())
        .await
        .unwrap();
    #[cfg(feature = "valetudo_debugging")]
    log("swagger_loader - written get command to TcpStream");
    let mut line = String::new();
    loop {
        match stream.read_line(&mut line).await {
            Ok(0) => {
                #[cfg(feature = "valetudo_debugging")]
                println!("valetudo swagger loader: reached EOF = Ok(0)");
                break;
            }
            Ok(_) => {
                if line.starts_with('[') || line.starts_with('{') {
                    // got what I was looking for. breaking out of loop & return it to caller
                    break;
                }
                #[cfg(feature = "valetudo_debugging")]
                print!("valetudo swagger loader, got non matching line: {line}");
                line.clear();
            }
            Err(e) => println!("valetudo swagger loader: Error = {e}"),
        }
    }
    #[cfg(feature = "valetudo_debugging")]
    log("swagger_loader - closing TcpStream now");
    stream.shutdown().await.ok();
    #[cfg(feature = "valetudo_debugging")]
    log("swagger_loader - stream closed, returning received json string now");
    line
}

pub async fn wifi_watcher(
    addr: SocketAddr,
    hostname: String,
    query_frequency: Duration,
    tx: Sender<Measurement>,
) {
    #[cfg(feature = "valetudo_debugging")]
    log("valetudo_wifi_watcher started");
    loop {
        let loaded = swagger_loader(
            &addr,
            "/api/v2/robot/capabilities/WifiConfigurationCapability",
        )
        .await;
        let time = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_nanos();
        if let Ok(value) = from_str::<JsonValue>(&loaded) {
            #[cfg(feature = "valetudo_debugging")]
            log(&format!(
                "valetudo_wifi_watcher - parsed data: {}",
                to_string_pretty(&value).unwrap()
            ));
            match value["details"]["signal"].as_f64() {
                Some(value) => {
                    let m = Measurement::new(
                        format!(" {hostname}.wifi"),
                        value,
                        time,
                        #[cfg(feature = "track_source")]
                        crate::MeasurementSource::Valetudo,
                    );
                    tx.send(m).await.ok();
                }
                None => {
                    log(&format!(
                        "valetudo_wifi_watcher - json didn't contain valid signal strength: {}",
                        to_string_pretty(&value).unwrap()
                    ));
                }
            }
        } else {
            log("valetudo_wifi_watcher - couldn't parse data: {loaded}");
        }
        sleep(query_frequency).await;
    }
}

pub async fn state_watcher(addr: SocketAddr, hostname: String, tx: Sender<Measurement>, minimum_frequency: Duration) {
    // first get current attributes
    valetudo_sender(
        &swagger_loader(&addr, "/api/v2/robot/state/attributes").await,
        &hostname,
        &tx,
    )
    .await;
    let mut last_sent = Instant::now();

    // then connect to the SSE source and send received updates
    let mut stream = valetudo_sse_subscribe(&addr).await;

    let mut line = String::new();
    #[cfg(feature = "valetudo_filter_unchanged")]
    let mut last_data = String::new();
    loop {
        let next_needed: Duration = minimum_frequency - Instant::now().duration_since(last_sent);
        match timeout(next_needed,stream.read_line(&mut line)).await {
            Ok(Ok(read)) => {
                if let Some(data) = line.strip_prefix("data: ") {
                    #[cfg(feature = "valetudo_filter_unchanged")]
                    if last_data == data && Instant::now().duration_since(last_sent) < minimum_frequency/3*2 {
                        #[cfg(feature = "valetudo_debugging")]
                        log("ignoring unchanged state");
                        continue;
                    }
                    #[cfg(feature = "valetudo_filter_unchanged")]
                    data.clone_into(&mut last_data);
                    #[cfg(feature = "valetudo_debugging")]
                    log(&format!("new state: {data}"));
                    valetudo_sender(data, &hostname, &tx).await;
                    last_sent = Instant::now();
                } else {
                    #[cfg(feature = "valetudo_debugging")]
                    print!("line without data prefix: {line}");
                }
                if read > 0 {
                    line.clear();
                } else {
                    // #[cfg(feature = "valetudo_debugging")]
                    log("read 0 bytes, that seems to mean connection broke. reconnecting");
                    stream.shutdown().await.ok();
                    stream = valetudo_sse_subscribe(&addr).await;
                }
            }
            Ok(Err(e)) => {
                log_error(&format!("failed to read line from valetudo_sse stream: {e}"));
            }
            Err(_e) => {
                #[cfg(feature = "valetudo_debugging")]
                log(&format!("no data line received within min_frequency, loading via http endpoint instead - {_e}"));
                valetudo_sender(
                    &swagger_loader(&addr, "/api/v2/robot/state/attributes").await,
                    &hostname,
                    &tx,
                )
                .await;
                last_sent = Instant::now();
            }
        } 
        // else {
        //     println!("couldn't read line from stream");
        // }
    }
}
