use crate::{metrics_sender::metrics_sender, Measurement};
use std::{fs, path::Path, time::Duration};
use tokio::{sync::mpsc::Sender, task::JoinHandle};

pub fn setup(
    tx: &Sender<Measurement>,
    skip_zones: Vec<u8>,
    host_name: &str,
    zone_devisor: f64,
    single: bool,
    query_frequency: Duration,
) -> Vec<JoinHandle<()>> {
    let mut handles = vec![];

    // https://stackoverflow.com/questions/9271970/how-do-you-make-a-range-in-rust
    for zone in 0..u8::MAX {
        let path = format!("/sys/class/thermal/thermal_zone{zone}/temp");
        if !Path::new(&path).exists() {
            #[cfg(feature = "thermal_zones_debugging")]
            println!("no zone #{zone} is available on this system, therefore with all likelihood neither is any with a bigger number");
            break;
        }
        if !skip_zones.contains(&zone) {
            let invalid_values = if zone == 11 { vec![0.05_f64] } else { vec![] };
            //handles.push(tokio::spawn(watch_file(path, zone, tx.clone(), invalid_values)));
            let prefix = if single {
                format!(" {host_name}.temperature")
            } else {
                format!(",zone={zone} {host_name}.temperature")
            };
            #[cfg(feature = "thermal_zones_debugging")]
            println!("spawning metrics_sender for thermal zone #{zone}");
            handles.push(tokio::spawn(metrics_sender(
                tx.clone(),
                prefix,
                invalid_values,
                query_frequency,
                move || {
                    let mut value = fs::read_to_string(&path);
                    while value.is_err() {
                        println!("error reading file: {path} - trying again in 400ms.");
                        std::thread::sleep(Duration::from_millis(400));
                        value = fs::read_to_string(&path);
                    }
                    value.unwrap().trim().parse::<f64>().unwrap() / zone_devisor
                },
                #[cfg(feature = "track_source")]
                crate::MeasurementSource::ThermalZones,
            )));
        }
    }
    handles
}
