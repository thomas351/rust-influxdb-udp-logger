#[cfg(all(feature = "alias", feature = "signal_notifier"))]
use crate::apply_aliases;
#[cfg(feature = "http_server")]
use crate::http_server;
#[cfg(any(
    feature = "sender_debugging",
    feature = "signal_notifier",
    feature = "subscription_sender"
))]
use crate::log;
#[cfg(feature = "track_source")]
use crate::MeasurementSource;
#[cfg(all(
    feature = "alias",
    any(feature = "signal_notifier", feature = "http_server")
))]
use crate::PrefixAlias;
#[cfg(feature = "signal_notifier")]
use crate::{
    format_nanos, format_now,
    human_readable_duration::format_duration,
    signal_notifier::{self, stale_detector},
};
use crate::{log_error, tcp_connect, Cli, HostnameSocketAddr, Measurement, Protocol};
#[cfg(feature = "signal_notifier")]
use chrono::DateTime;
use float_cmp::approx_eq;
#[cfg(feature = "http_sender")]
use std::str;
#[cfg(feature = "subscription_sender")]
use std::str::FromStr;
#[cfg(any(feature = "auto_resend", feature = "signal_notifier"))]
use std::time::SystemTime;
#[cfg(feature = "auto_resend")]
use std::{cmp::min, time::UNIX_EPOCH};
use std::{
    collections::HashMap,
    net::{Ipv4Addr, SocketAddr},
    sync::Arc,
    time::Duration,
};
#[cfg(feature = "signal_notifier")]
use tokio::sync::mpsc::Sender;
use tokio::{
    io::AsyncWriteExt,
    net::{TcpStream, UdpSocket},
    sync::{mpsc::Receiver, RwLock},
    time::{sleep, timeout},
};
#[cfg(feature = "subscription_sender")]
use tokio::{io::BufStream, net::TcpListener};

#[cfg(feature = "http_sender")]
fn build_http_request(
    client: &reqwest::Client,
    url: &str,
    user: &Option<String>,
    password: &Option<String>,
) -> reqwest::RequestBuilder {
    let builder = client.post(url);
    if let Some(user) = user {
        builder.basic_auth(user, password.as_ref())
    } else {
        builder
    }
}

async fn open_udp_connection(target: &SocketAddr) -> UdpSocket {
    // https://stackoverflow.com/questions/65130849/how-do-i-connect-an-udp-socket-and-let-the-os-choose-the-port-and-address
    // addr: (UNSPECIFIED, 0) means system decides ip and port used
    let sock = UdpSocket::bind((Ipv4Addr::UNSPECIFIED, 0)).await.unwrap();
    println!("bound socket: {:?}", sock);

    let mut connected = sock.connect(target).await;
    while connected.is_err() {
        println!("error connecting to victoriametrics influxdb port 8089: {connected:?}");
        sleep(Duration::from_millis(400)).await;
        connected = sock.connect(target).await;
    }
    sock
}

#[cfg(feature = "signal_notifier")]
const CHANGEABLE_TAGS: &'static [&'static str] = &["state", "status", "error"];

#[cfg(feature = "signal_notifier")]
fn illegal_tag_change_mono(tags1: &Vec<&str>, tags2: &Vec<&str>) -> bool {
    for tag in tags1 {
        let mut tag_accepted = false;
        for changeable_tag in CHANGEABLE_TAGS {
            if tag.contains(changeable_tag) {
                // this tag is allowed to change and still be considered the same metric
                tag_accepted = true;
                break;
            }
        }
        for other_tag in tags2 {
            if other_tag == tag {
                // tag didn't change
                tag_accepted = true;
                break;
            }
        }
        if !tag_accepted {
            // unchangeable tag changed or added
            return true;
        }
    }
    false
}
#[cfg(feature = "signal_notifier")]
fn illegal_tag_change(tags1: &Vec<&str>, tags2: &Vec<&str>) -> bool {
    illegal_tag_change_mono(tags1, tags2) || illegal_tag_change_mono(tags2, tags1)
}

#[cfg(feature = "signal_notifier")]
fn same_metric(prefix1: &str, prefix2: &str) -> bool {
    if let Some((tags1, metric1)) = prefix1.rsplit_once(' ')
        && let Some((tags2, metric2)) = prefix2.rsplit_once(' ')
        && metric1 == metric2
    {
        let tags1a: Vec<&str> = tags1.split(',').collect();
        let tags2a: Vec<&str> = tags2.split(',').collect();
        !illegal_tag_change(&tags1a, &tags2a)
    } else {
        false
    }
}

trait DataSender {
    async fn send(&mut self, m: &Measurement);
}

struct StdoutSender {
    ignore_clock: bool,
}
impl DataSender for StdoutSender {
    async fn send(&mut self, m: &Measurement) {
        print!("{}", m.format(self.ignore_clock));
    }
}

struct MultiSender {
    senders: Vec<SimpleSenderEnum>,
}
impl MultiSender {
    fn new(senders: Vec<SimpleSenderEnum>) -> Self {
        MultiSender { senders }
    }
    // fn add_sender(&mut self, sender: SimpleSenderEnum) {
    //     self.senders.push(sender);
    // }
}
impl DataSender for MultiSender {
    async fn send(&mut self, m: &Measurement) {
        for sender in self.senders.iter_mut() {
            sender.send(m).await;
        }
    }
}

#[cfg(feature = "subscription_sender")]
struct TcpSubscriptionSender {
    subscribers: Arc<RwLock<Vec<BufStream<TcpStream>>>>,
}
#[cfg(feature = "subscription_sender")]
impl TcpSubscriptionSender {
    async fn new(port: u16) -> Self {
        let subscribers = Arc::new(RwLock::new(vec![]));
        let sender = TcpSubscriptionSender {
            subscribers: subscribers.clone(),
        };
        tokio::spawn(TcpSubscriptionSender::listener_loop(port, subscribers));
        sender
    }

    async fn listener_loop(port: u16, subscribers: Arc<RwLock<Vec<BufStream<TcpStream>>>>) {
        let mut addr_str = port.to_string();
        addr_str.insert_str(0, "0.0.0.0:");
        let listen_on: HostnameSocketAddr =
            HostnameSocketAddr::from_str(addr_str.as_str()).unwrap();
        let listener = TcpListener::bind(listen_on.addr).await.unwrap();
        loop {
            match listener.accept().await {
                Ok((stream, remote)) => {
                    log(&format!("TcpSubscriptionSender new connection: {remote}"));
                    subscribers.write().await.push(BufStream::new(stream));
                }
                Err(e) => log_error(&format!(
                    "influxdb tcp_listener incomming connection failed: {e}"
                )),
            }
        }
    }
}
#[cfg(feature = "subscription_sender")]
impl DataSender for TcpSubscriptionSender {
    async fn send(&mut self, m: &Measurement) {
        let mut lock = self.subscribers.write().await;
        let mut working_subscribers = vec![];
        for mut subscriber in lock.drain(..) {
            match tokio::time::timeout(
                Duration::from_millis(500),
                subscriber.write(m.to_string().as_bytes()),
            )
            .await
            {
                Ok(Ok(_bytes_written)) => {
                    tokio::time::timeout(Duration::from_millis(500), subscriber.flush())
                        .await
                        .ok();
                    working_subscribers.push(subscriber);
                }
                Ok(Err(e)) => {
                    log_error(&format!("TcpSubscriptionSender subscriber.write failed: {e} - removing from subscribers!"));
                }
                Err(e) => {
                    log_error(&format!("TcpSubscriptionSender subscriber.write timedout after {e} - removing from subscribers, need to keep it fast to prevent data from beeing dropped!"));
                }
            }
        }
        lock.append(&mut working_subscribers);
    }
}

struct TcpSender {
    stream: TcpStream,
    addr: SocketAddr,
    ignore_clock: bool,
}
impl TcpSender {
    async fn new(addr: SocketAddr, ignore_clock: bool) -> Self {
        TcpSender {
            stream: tcp_connect(&addr).await,
            addr,
            ignore_clock,
        }
    }
}
impl DataSender for TcpSender {
    async fn send(&mut self, m: &Measurement) {
        let s = m.format(self.ignore_clock);
        let mut result = self.stream.write(s.as_bytes()).await;
        while result.is_err() {
            eprintln!("error sending line to victoriametrics: {result:?}");
            self.stream.shutdown().await.ok(); //don't care if shutdown fails
            self.stream = tcp_connect(&self.addr).await;
            result = self.stream.write(s.as_bytes()).await;
        }
    }
}

struct UdpSender {
    sock: UdpSocket,
    addr: SocketAddr,
    ignore_clock: bool,
}
impl UdpSender {
    async fn new(addr: SocketAddr, ignore_clock: bool) -> Self {
        UdpSender {
            sock: open_udp_connection(&addr).await,
            addr,
            ignore_clock,
        }
    }
}
impl DataSender for UdpSender {
    async fn send(&mut self, m: &Measurement) {
        let s = m.format(self.ignore_clock);
        let mut result = self.sock.send(s.as_bytes()).await;
        while result.is_err() {
            let err = format!("UdpSender socket.send failed! Recreating socket. Error: {result:?}");
            log_error(&err);
            self.sock = open_udp_connection(&self.addr).await;
            result = self.sock.send(s.as_bytes()).await;
        }
    }
}

#[cfg(feature = "http_sender")]
struct HttpSender {
    client: reqwest::Client,
    url: String,
    user: Option<String>,
    password: Option<String>,
    ignore_clock: bool,
}
#[cfg(feature = "http_sender")]
impl HttpSender {
    async fn new(
        url: String,
        user: Option<String>,
        password: Option<String>,
        ignore_clock: bool,
    ) -> Self {
        HttpSender {
            client: reqwest::Client::new(),
            url,
            user,
            password,
            ignore_clock,
        }
    }
}
#[cfg(feature = "http_sender")]
impl DataSender for HttpSender {
    async fn send(&mut self, m: &Measurement) {
        let mut result = build_http_request(&self.client, &self.url, &self.user, &self.password)
            .body(m.format(self.ignore_clock))
            .send()
            .await;
        while result.is_err() {
            eprintln!("error sending line to victoriametrics: {result:?}");
            self.client = reqwest::Client::new();
            result = build_http_request(&self.client, &self.url, &self.user, &self.password)
                .body(m.format(self.ignore_clock))
                .send()
                .await;
        }
    }
}

enum SimpleSenderEnum {
    StdOut(StdoutSender),
    Tcp(TcpSender),
    Udp(UdpSender),
    #[cfg(feature = "http_sender")]
    Http(HttpSender),
    #[cfg(feature = "subscription_sender")]
    TcpSubscription(TcpSubscriptionSender),
}
impl DataSender for SimpleSenderEnum {
    async fn send(&mut self, m: &Measurement) {
        match self {
            SimpleSenderEnum::StdOut(x) => x.send(m).await,
            SimpleSenderEnum::Tcp(x) => x.send(m).await,
            SimpleSenderEnum::Udp(x) => x.send(m).await,
            #[cfg(feature = "http_sender")]
            SimpleSenderEnum::Http(x) => x.send(m).await,
            #[cfg(feature = "subscription_sender")]
            SimpleSenderEnum::TcpSubscription(x) => x.send(m).await,
        }
    }
}
enum DataSenderEnum {
    Simple(SimpleSenderEnum),
    Multi(MultiSender),
}
impl DataSender for DataSenderEnum {
    async fn send(&mut self, m: &Measurement) {
        match self {
            DataSenderEnum::Simple(x) => x.send(m).await,
            DataSenderEnum::Multi(x) => x.send(m).await,
        }
    }
}
impl DataSenderEnum {
    async fn create(
        target: Option<HostnameSocketAddr>,
        protocol: Protocol,
        #[cfg(feature = "http_sender")] http_user: Option<String>,
        #[cfg(feature = "http_sender")] http_password: Option<String>,
        #[cfg(feature = "http_sender")] http_url: Option<String>,
        stdout_too: bool,
        #[cfg(feature = "subscription_sender")] subscription_port: Option<u16>,
        ignore_clock: bool,
    ) -> Self {
        #[cfg(feature = "sender_debugging")]
        log("get_sender started");

        let mut senders = vec![];
        if stdout_too {
            senders.push(SimpleSenderEnum::StdOut(StdoutSender { ignore_clock }));
        }
        #[cfg(feature = "subscription_sender")]
        if let Some(port) = subscription_port {
            senders.push(SimpleSenderEnum::TcpSubscription(
                TcpSubscriptionSender::new(port).await,
            ));
        }
        // if no target & not http: return DataSenderEnum::StdOut(StdoutSender);

        match (target, protocol) {
            (None, _) => {
                #[cfg(feature = "http_sender")]
                if protocol == Protocol::Http
                    && let Some(url) = http_url
                {
                    senders.push(SimpleSenderEnum::Http(
                        HttpSender::new(url, http_user, http_password, ignore_clock).await,
                    ));
                }
            }
            (Some(addr), Protocol::Udp) => {
                senders.push(SimpleSenderEnum::Udp(
                    UdpSender::new(addr.addr, ignore_clock).await,
                ));
            }
            (Some(addr), Protocol::Tcp) => {
                senders.push(SimpleSenderEnum::Tcp(
                    TcpSender::new(addr.addr, ignore_clock).await,
                ));
            }
            #[cfg(feature = "http_sender")]
            (Some(addr), Protocol::Http) => {
                let url = match http_url {
                    Some(url) => url,
                    None => {
                        format!("https://{}:{}/write", &addr.addr.ip(), &addr.addr.port())
                    }
                };
                senders.push(SimpleSenderEnum::Http(
                    HttpSender::new(url, http_user, http_password, ignore_clock).await,
                ));
            }
        };

        match senders.len() {
            0..1 => DataSenderEnum::Simple(
                senders
                    .pop()
                    .unwrap_or(SimpleSenderEnum::StdOut(StdoutSender { ignore_clock })),
            ),
            _ => DataSenderEnum::Multi(MultiSender::new(senders)),
        }
    }
}

pub struct MeasurementCache {
    pub value: f64,
    pub last_sent: u128,
    pub last_measured: u128,
    #[cfg(feature = "track_source")]
    pub src: MeasurementSource,
    #[cfg(feature = "signal_notifier")]
    pub count: usize,
    #[cfg(feature = "signal_notifier")]
    pub avg_period_nanos: u64,
    #[cfg(feature = "signal_notifier")]
    pub stale_notification_sent: bool,
    #[cfg(feature = "signal_notifier")]
    pub watching: Option<bool>,
}

impl MeasurementCache {
    fn new(
        value: f64,
        time: u128,
        #[cfg(feature = "track_source")] src: MeasurementSource,
    ) -> Self {
        MeasurementCache {
            value,
            last_sent: time,
            last_measured: time,
            #[cfg(feature = "track_source")]
            src,
            #[cfg(feature = "signal_notifier")]
            count: 1,
            #[cfg(feature = "signal_notifier")]
            avg_period_nanos: 0,
            #[cfg(feature = "signal_notifier")]
            stale_notification_sent: false,
            #[cfg(feature = "signal_notifier")]
            watching: None
        }
    }
}

pub struct SenderCache {
    pub measurements: HashMap<String, MeasurementCache>,
    pub sender_idle: bool,
    #[cfg(all(
        feature = "alias",
        any(feature = "signal_notifier", feature = "http_server")
    ))]
    pub prefix_aliases: Vec<PrefixAlias>,
}

#[cfg(all(
    feature = "alias",
    any(feature = "signal_notifier", feature = "http_server")
))]
impl SenderCache {
    pub fn mut_borrow(&mut self) -> (&Vec<PrefixAlias>, &mut HashMap<String, MeasurementCache>) {
        (&self.prefix_aliases, &mut self.measurements)
    }
}

#[cfg(feature = "auto_resend")]
pub struct ResendEntry {
    pub value: f64,
    pub time: u128,
    #[cfg(feature = "track_source")]
    pub src: MeasurementSource,
}

pub(crate) async fn sender_loop(
    mut rx: Receiver<Measurement>,
    args: Cli,
    #[cfg(feature = "signal_notifier")] signal_tx: Sender<String>,
    #[cfg(feature = "signal_notifier")] signal_rx: Receiver<String>,
) {
    #[cfg(feature = "sender_debugging")]
    log("data_sender started");
    let mut sender = DataSenderEnum::create(
        args.target,
        args.protocol,
        #[cfg(feature = "http_sender")]
        args.http_user,
        #[cfg(feature = "http_sender")]
        args.http_password,
        #[cfg(feature = "http_sender")]
        args.http_url,
        args.stdout_too,
        #[cfg(feature = "subscription_sender")]
        args.subscription_port,
        args.ignore_clock,
    )
    .await;

    // #[cfg(feature = "http_server")]
    let cache: Arc<RwLock<SenderCache>> = Arc::new(RwLock::new(SenderCache {
        measurements: HashMap::new(),
        sender_idle: false,
        #[cfg(all(
            feature = "alias",
            any(feature = "signal_notifier", feature = "http_server")
        ))]
        prefix_aliases: args.prefix_alias,
    }));

    #[cfg(feature = "signal_notifier")]
    let started = SystemTime::now();
    #[cfg(feature = "signal_notifier")]
    let _signal_notifier = args.signal_group.map(|group| {
        log(&format!(
            "signal_notifier activated, target group = '{group}'"
        ));
        tokio::spawn(signal_notifier::signal_cli_controller(group, signal_rx))
    });
    #[cfg(feature = "signal_notifier")]
    signal_tx
        .try_send(format!(
            "rust-influxdb-logger started @ {} - skipping new metric notifications for first {}",
            format_now(),
            format_duration(Duration::from_secs(args.skip_new_metrics_notification_sec))
        ))
        .ok();
    #[cfg(feature = "signal_notifier")]
    let _stale_detector = tokio::spawn(stale_detector(
        Arc::clone(&cache),
        signal_tx.clone(),
        args.min_staleness_sec,
        args.staleness_factor,
    ));
    #[cfg(feature = "signal_notifier")]
    if !args.signal_watch.is_empty() {
        log(&format!("signal watch: {:?}", args.signal_watch));
        // args.signal_watch.sort_unstable();
    }

    // #[cfg(not(feature = "http_server"))]
    // let mut last_sent: HashMap<String, (f64, u128)> = HashMap::new();

    // let mut last_not_sent: HashMap<String, (f64, u128)> = HashMap::new();
    #[cfg(feature = "auto_resend")]
    let mut auto_resend: HashMap<String, ResendEntry> = HashMap::new();
    #[cfg(feature = "auto_resend")]
    let mut next_resend = u64::MAX;
    #[cfg(not(feature = "auto_resend"))]
    let next_resend = u64::MAX;

    #[cfg(feature = "http_server")]
    let http_server = args
        .http_status_port
        .map(|port| tokio::spawn(http_server::listener(port, Arc::clone(&cache))));

    let min_freq_nanos = 1_000_000_000 * (args.minimum_frequency as u64);
    loop {
        cache.write().await.sender_idle = rx.is_empty();
        match timeout(Duration::from_nanos(next_resend), rx.recv()).await {
            Ok(Some(m)) => {
                let mut cache_lock = cache.write().await;
                cache_lock.sender_idle = false;

                #[cfg(feature = "auto_resend")]
                let mut sent_time = m.time;

                #[cfg(all(
                    feature = "alias",
                    any(feature = "signal_notifier", feature = "http_server")
                ))]
                let (prefix_aliases, measurements) = cache_lock.mut_borrow();
                #[cfg(not(all(
                    feature = "alias",
                    any(feature = "signal_notifier", feature = "http_server")
                )))]
                let measurements = &mut cache_lock.measurements;
                if let Some(mut c) = measurements.remove(&m.prefix) {
                    #[cfg(feature = "signal_notifier")]
                    if c.stale_notification_sent {

                        #[cfg(all(feature = "alias", feature = "signal_notifier"))]
                        let aliased_prefix = apply_aliases(&m.prefix, prefix_aliases);
                        #[cfg(not(all(feature = "alias", feature = "signal_notifier")))]
                        let aliased_prefix = &m.prefix;

                        // detect if tag change and adapt notification text accordingly
                        let mut msg = if let Some((pre_change_prefix, pre_change_cache)) =
                            measurements.iter_mut().find(|(p, mc)| {
                                // use aliased prefixes because we use the alias feature to map tags that distinquish different devices to different metrics
                                same_metric(&apply_aliases(p, prefix_aliases), &aliased_prefix)
                                    && !mc.stale_notification_sent
                            }) {
                            // this is an existing metric with changed tags:
                            // * mark previous version as "stale_notification_sent"
                            pre_change_cache.stale_notification_sent = true;
                            // & adapt notification text to state the change
                            format!(
                                "tags changed back: {}={} @{} -> {}={} @{}; last={} @{}",
                                apply_aliases(pre_change_prefix, prefix_aliases),
                                pre_change_cache.value,
                                format_nanos(pre_change_cache.last_measured as _),
                                aliased_prefix,
                                m.value,
                                format_nanos(m.time as _),
                                c.value,
                                format_nanos(c.last_measured as _)
                            )
                        } else {
                            format!(
                                "stale metric reapeared: {}={} ; before stale={} @{}",
                                aliased_prefix,
                                m.value,
                                c.value,
                                format_nanos(c.last_measured as _)
                            )
                        };
                        #[cfg(feature = "track_source")]
                        {
                            msg += " from ";
                            msg += &m.src.to_string();
                        }
                        signal_tx.try_send(msg).ok();
                        c.count = 1;
                        c.avg_period_nanos = 0;
                        c.stale_notification_sent = false;
                    } else {
                        c.avg_period_nanos = (c.avg_period_nanos * (c.count - 1) as u64
                            + (m.time - c.last_measured) as u64)
                            / c.count as u64;
                        c.count += 1;
                    }

                    if approx_eq!(f64, m.value, c.value)
                        && m.time < c.last_sent + min_freq_nanos as u128
                    {
                        // don't send unchanged values unless minimum_frequency requires it
                        #[cfg(feature = "auto_resend")]
                        {
                            sent_time = c.last_sent;
                        }
                        c.last_measured = m.time;
                    } else {
                        if c.last_sent != c.last_measured && !args.ignore_clock {
                            // send last unsent value before change to sharpen contrast of when the value changed
                            sender
                                .send(&Measurement::new(
                                    m.prefix.clone(),
                                    c.value,
                                    c.last_measured,
                                    #[cfg(feature = "track_source")]
                                    c.src.clone(),
                                ))
                                .await;
                        }

                        #[cfg(feature = "signal_notifier")]
                        if !approx_eq!(f64, m.value, c.value) {
                            if c.watching.is_none() {
                                #[cfg(all(feature = "alias", feature = "signal_notifier"))]
                                let aliased_prefix = apply_aliases(&m.prefix, prefix_aliases);
                                #[cfg(not(all(feature = "alias", feature = "signal_notifier")))]
                                let aliased_prefix = &m.prefix;
                                for watch in &args.signal_watch {
                                    if m.prefix.eq(watch) || aliased_prefix.eq(watch) {
                                        c.watching = Some(true);
                                        break;
                                    }
                                    if let Ok(regex) = regex::Regex::new(watch) 
                                        && (regex.captures(&m.prefix).is_some() || 
                                            regex.captures(&aliased_prefix).is_some()) {
                                        c.watching = Some(true);
                                        break;
                                    }
                                }
                                if c.watching.is_none() {
                                    c.watching = Some(false);
                                }
                                #[cfg(feature = "signal_watch_debugging")] {
                                    if aliased_prefix.eq(&m.prefix) {
                                        log(&format!("{}: first value change, checked if on watchlist: {}", m.prefix, c.watching.unwrap()));
                                    } else {
                                        log(&format!("{} / {}: first value change, checked if on watchlist: {}", m.prefix, aliased_prefix, c.watching.unwrap()));
                                    }
                                }
                                
                            }
                            
                            // #[cfg(feature = "signal_watch_debugging")] {
                            //     let msg = format!("{aliased_prefix} changed: {} -> {}", c.value, m.value);
                            //     log(&msg);
                            //     if let Some(true) = c.watching {
                            //         signal_tx.try_send(msg).ok();
                            //     }
                            // }
                            // #[cfg(not(feature = "signal_watch_debugging"))]

                            if let Some(true) = &c.watching {
                                #[cfg(all(feature = "alias", feature = "signal_notifier"))]
                                let aliased_prefix = apply_aliases(&m.prefix, prefix_aliases);
                                #[cfg(not(all(feature = "alias", feature = "signal_notifier")))]
                                let aliased_prefix = &m.prefix;
                                signal_tx.try_send(format!("{aliased_prefix} changed: {} -> {}", c.value, m.value)).ok();
                            }
                        }
                        // #[cfg(feature = "signal_notifier")]
                        // {
                        //     let diff = (m.time - c.last_measured) as u64;
                        //     if diff > (args.signal_notify_pause_end_minutes * 60 * 1_000_000_000)
                        //         && m.time > c.last_measured
                        //     {
                        //         // pause has been longer than signal_notify_pause_end_minutes setting, notify!
                        //         notification = Some(format!(
                        //             "metric reappeared after {}: {} was {} @{}, now {} @{}",
                        //             format_duration(Duration::from_nanos(diff)),
                        //             m.prefix,
                        //             c.value,
                        //             format_nanos(c.last_measured as _),
                        //             m.value,
                        //             format_nanos(m.time as _),
                        //         ));
                        //     }
                        // }

                        // finally send the measurement and update cache
                        sender.send(&m).await;
                        c.value = m.value;
                        c.last_sent = m.time;
                        c.last_measured = m.time;
                        #[cfg(all(feature = "auto_resend", feature = "track_source"))]
                        {
                            c.src = m.src.clone();
                        }
                        #[cfg(all(not(feature = "auto_resend"), feature = "track_source"))]
                        {
                            c.src = m.src;
                        }
                    }
                    //readd modified measurement cache (removed it so we can mutably iterate over map even while holding the current's measurement's cache)
                    measurements.insert(m.prefix.clone(), c);
                } else {
                    // this prefix has not been sent yet.
                    // first time measurement! send & create cache entry!
                    // tokio::spawn();
                    sender.send(&m).await;
                    #[cfg(feature = "signal_notifier")]
                    if args.skip_new_metrics_notification_sec < started.elapsed().unwrap().as_secs()
                    {
                        #[cfg(feature = "alias")]
                        let prefix = apply_aliases(&m.prefix, &cache_lock.prefix_aliases);
                        #[cfg(not(feature = "alias"))]
                        let prefix = &m.prefix;
                        let (aliases, cache) = cache_lock.mut_borrow();
                        let mut msg = if let Some((op, mc)) = cache.iter_mut().find(|(p, mc)| {
                            // use aliased prefixes because we use the alias feature to map tags that distinquish different devices to different metrics
                            same_metric(&apply_aliases(p, aliases), &prefix)
                                && !mc.stale_notification_sent
                        }) {
                            // this is an existing metric with changed tags:
                            // * mark previous version as "stale_notification_sent"
                            mc.stale_notification_sent = true;
                            // & adapt notification text to state the change
                            format!(
                                "new tags change: {}={} @{} -> {}={} @{}",
                                apply_aliases(op, aliases),
                                mc.value,
                                DateTime::from_timestamp_nanos(mc.last_measured as _)
                                    .format("%Y-%m-%d %H:%M:%S%.6f"),
                                prefix,
                                m.value,
                                DateTime::from_timestamp_nanos(m.time as _)
                                    .format("%Y-%m-%d %H:%M:%S%.6f")
                            )
                        } else {
                            format!(
                                "new metric: {}={} @{}",
                                prefix,
                                m.value,
                                DateTime::from_timestamp_nanos(m.time as _)
                                    .format("%Y-%m-%d %H:%M:%S%.6f")
                            )
                        };
                        #[cfg(feature = "track_source")]
                        {
                            msg += " from ";
                            msg += &m.src.to_string();
                        }
                        signal_tx.send(msg).await.ok();
                    }
                    cache_lock.measurements.insert(
                        m.prefix.clone(),
                        MeasurementCache::new(
                            m.value,
                            m.time,
                            #[cfg(all(
                                any(feature = "auto_resend", feature = "signal_notifier"),
                                feature = "track_source"
                            ))]
                            m.src.clone(),
                            #[cfg(all(
                                not(any(feature = "auto_resend", feature = "signal_notifier")),
                                feature = "track_source"
                            ))]
                            m.src,
                        ),
                    );
                }

                #[cfg(feature = "auto_resend")]
                if m.auto_resend {
                    auto_resend.insert(
                        m.prefix,
                        ResendEntry {
                            value: m.value,
                            time: sent_time,
                            #[cfg(feature = "track_source")]
                            src: m.src,
                        },
                    );
                } else {
                    auto_resend.remove(&m.prefix);
                }
            }
            Ok(None) => break,
            Err(_e) => {
                #[cfg(feature = "sender_debugging")]
                log(&format!("rx.recv() timeout: {_e}"));
            }
        }
        #[cfg(feature = "auto_resend")]
        {
            next_resend = u64::MAX;
            let now = SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .unwrap()
                .as_nanos();
            for (prefix, e) in &auto_resend {
                if e.time + min_freq_nanos as u128 <= now {
                    let m = Measurement::new(
                        prefix.into(),
                        e.value,
                        now,
                        #[cfg(feature = "track_source")]
                        e.src.clone(),
                    );
                    sender.send(&m).await;
                    next_resend = min(next_resend, min_freq_nanos);
                } else {
                    next_resend = min(next_resend, ((e.time + min_freq_nanos as u128) - now) as _);
                }
            }
        }
    }
    log_error("data_sender ended!?");
    #[cfg(feature = "http_server")]
    if let Some(server) = http_server {
        server.abort();
    };
}
