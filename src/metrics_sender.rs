#[cfg(feature = "metrics_sender_debugging")]
use crate::log;
use crate::{log_error, Measurement};
use float_cmp::approx_eq;
use std::time::Duration;
use std::time::{SystemTime, UNIX_EPOCH};
use tokio::sync::mpsc::Sender;
use tokio::time::sleep;

pub async fn metrics_sender(
    tx: Sender<Measurement>,
    prefix: String,
    invalid_values: Vec<f64>,
    query_frequency: Duration,
    mut value_getter: impl FnMut() -> f64,
    #[cfg(feature = "track_source")] src: crate::MeasurementSource,
) {
    #[cfg(feature = "metrics_sender_debugging")]
    println!("metrics_sender setup for prefix {prefix}");
    loop {
        let value = value_getter();
        #[cfg(feature = "metrics_sender_debugging")]
        log(&format!("metrics_sender {prefix} read value: {value}"));
        let mut invalid = false;
        for invalid_value in &invalid_values {
            if approx_eq!(f64, *invalid_value, value) {
                invalid = true;
                break;
            }
        }
        if !invalid {
            let time = SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .unwrap()
                .as_nanos();
            let m = Measurement::new(
                prefix.clone(),
                value,
                time,
                #[cfg(feature = "track_source")]
                src.clone(),
            );
            if let Err(e) = tx.send(m).await {
                log_error(&format!("metrics_sender tx.send failed: {e}"));
            }
        }
        sleep(query_frequency).await;
    }
}

#[cfg(feature = "external")]
pub async fn string_sender(
    tx: Sender<Measurement>,
    prefix: String,
    query_frequency: Duration,
    mut getter: impl FnMut() -> String,
) {
    #[cfg(feature = "metrics_sender_debugging")]
    println!("metrics_sender setup for prefix {prefix}");
    loop {
        let retrieved = getter();
        let time = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_nanos();

        #[cfg(feature = "metrics_sender_debugging")]
        log(&format!("metrics_sender {prefix} read: {retrieved}"));

        let parts: Vec<&str> = retrieved.split('=').collect();
        if parts.is_empty() {
            log_error(&format!("getter for {prefix} gave empty result!"));
            continue;
        }
        let last_index = parts.len() - 1;
        let Ok(value) = parts[last_index].trim().parse() else {
            log_error(&format!("getter for {prefix} gave bad result: {retrieved}"));
            continue;
        };
        let mut current_prefix = parts[..last_index].join("=");
        current_prefix.insert_str(0, &prefix);
        let m = Measurement::new(
            current_prefix,
            value,
            time,
            #[cfg(feature = "track_source")]
            crate::MeasurementSource::ExternalCommand,
        );
        if let Err(e) = tx.send(m).await {
            log_error(&format!("metrics_sender tx.send failed: {e}"))
        }
        sleep(query_frequency).await;
    }
}
