#[cfg(feature = "alias")]
use crate::apply_aliases;
use crate::{
    data_sender::SenderCache, format_nanos, format_now, human_readable_duration::format_duration,
    log, log_error,
};
use core::time::Duration;
use std::{
    io::Read,
    process::Command,
    sync::Arc,
    time::{SystemTime, UNIX_EPOCH},
};
use tokio::{
    sync::{
        mpsc::{Receiver, Sender},
        RwLock,
    },
    time::sleep,
};
use wait_timeout::ChildExt;

pub async fn stale_detector(
    src: Arc<RwLock<SenderCache>>,
    tx: Sender<String>,
    min_staleness_sec: u64,
    staleness_factor: f64,
) {
    loop {
        sleep(Duration::from_millis(1000)).await;
        if !src.read().await.sender_idle {
            continue;
        }
        let now = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_nanos();
        let mut cache_lock = src.write().await;

        #[cfg(feature = "alias")]
        let (prefix_aliases, measurements) = cache_lock.mut_borrow();
        #[cfg(not(feature = "alias"))]
        let measurements = &mut cache_lock.measurements;
        for (prefix, m) in measurements {
            let elapsed = now - m.last_measured;
            if m.avg_period_nanos == 0
                || (m.avg_period_nanos as f64 * staleness_factor) > elapsed as f64
                || m.stale_notification_sent
                || Duration::from_nanos(elapsed as _) < Duration::from_secs(min_staleness_sec)
                || m.last_measured > now
            {
                continue;
            }
            #[cfg(feature = "alias")]
            let applied = apply_aliases(prefix, prefix_aliases);
            #[cfg(not(feature = "alias"))]
            let applied = prefix;
            tx.send(format!(
                "metric stale: {}, measured {} times, average period = {}, last value = {} from {} (={} ago), from src={} (now={})",
                applied,
                m.count,
                format_duration(Duration::from_nanos(m.avg_period_nanos)),
                m.value,
                format_nanos(m.last_measured as _),
                format_duration(Duration::from_nanos(elapsed as _)),
                m.src,
                format_now(),
            ))
            .await
            .ok();
            m.stale_notification_sent = true;
        }
    }
}

pub async fn signal_cli_controller(group: String, mut rx: Receiver<String>) {
    loop {
        #[cfg(feature = "signal_receive")]
        if rx.is_empty() {
            receive_msgs(&group, &rx);
        } else {
            notification_sender(&group, &mut rx).await;
        }
        #[cfg(not(feature = "signal_receive"))]
        notification_sender(&group, &mut rx).await;
    }
}

pub async fn notification_sender(group: &str, rx: &mut Receiver<String>) {
    let mut msgs = Vec::new();
    rx.recv_many(&mut msgs, usize::MAX).await;
    let mut msg = String::new();
    for x in msgs.drain(..) {
        msg.push_str(&x);
        msg.push_str("\n\n");
    }
    send_notification(group, msg.strip_suffix("\n\n").unwrap(), &rx);
}

#[cfg(feature = "signal_receive")]
fn receive_msgs(authorized_group: &str, rx: &Receiver<String>) {
    // this is not yet functional sadly, we're struggling with this issue:
    // https://github.com/AsamK/signal-cli/issues/1532
    // `signal-cli receive` does:
    //   * use 100% of one cpu core / thread.
    //   * print sender's names and phone numbers, but no message content (decryption problem) and not if received as direct-message or via a group and if so which group
    //   * not finish running without terminate or kill signals (15 or 9) - it seems it's meant to continuously listen to incomming messages and stream them to stdout
    // our code does not yet process (log) messages while signal-cli is running, but only once it's killed. we don't process = intercept stderr stream at all, therefore it's directly output
    log("signal notifier: no msgs need sending, receiving instead");
    let output = execute_signal_cli(&mut vec!["receive"], rx);
    log(&format!("signal-cli receive output: {output} - TODO process line by line and check for msgs from authorized group = {authorized_group} - if there are any, reply with command unknown / list of allowed commands"));
}

fn send_notification(target_group: &str, msg: &str, rx: &Receiver<String>) {
    log(&format!("signal notify send msg: {msg}"));
    let output = execute_signal_cli(&mut vec!["send", "-g", target_group, "-m", msg], rx);
    log(&format!("signal-cli send output: {output}"));
}

fn execute_signal_cli(args: &mut Vec<&str>, rx: &Receiver<String>) -> String {
    let mut cmd = Command::new("signal-cli");
    #[cfg(feature = "signal_cli_debugging")]
    args.splice(0..0, ["-v"]);

    let mut child = match cmd.args(args).stdout(std::process::Stdio::piped()).spawn() {
        Ok(c) => c,
        Err(e) => {
            return format!("failed to spawn signal-cli: {e}");
        }
    };
    let mut stdout = match child.stdout.take() {
        Some(s) => s,
        None => {
            return "signal-cli: failed to acquire stdout from child!".into();
        }
    };
    let mut end = false;
    while !end {
        let done = match child.wait_timeout(Duration::from_secs(10)) {
            Ok(Some(es)) => {
                log(&format!("signal-cli finished, ExitStatus = {es}"));
                true
            }
            Ok(None) => {
                #[cfg(feature = "signal_cli_debugging")]
                log("signal-cli didn't finish running.");
                false
            }
            Err(e) => {
                log_error(&format!("signal-cli child.wait_timeout failed! {e}"));
                false
            }
        };
        if !rx.is_empty() && !done {
            log("new msgs need sending and previous signal-cli instance did not finish! killing it now!");
            if let Err(e) = child.kill() {
                log_error(&format!("failed to kill signal-cli: {e}"));
            } else {
                match child.wait() {
                    Ok(ec) => log(&format!("signal-cli kill succeeded, ExitStatus: {ec:?}")),
                    Err(e) => log_error(&format!("signal-cli wait after kill failed: {e}")),
                }
            }
        }
        end = done || !rx.is_empty();
    }

    let mut output = String::new();
    match stdout.read_to_string(&mut output) {
        Ok(count) => log(&format!("signal-cli output {count} bytes")),
        Err(e) => log_error(&format!(
            "signal-cli error while reading output, error: {e}"
        )),
    }
    output
}
