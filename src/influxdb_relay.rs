#[cfg(feature = "track_source")]
use crate::MeasurementSource;
use crate::{format_nanos, format_now, log, log_error, HostnameSocketAddr, Measurement};
use lazy_static::lazy_static;
use regex::Regex;
use std::{
    net::SocketAddr,
    str::FromStr,
    time::{Duration, SystemTime, UNIX_EPOCH},
};
use tokio::{
    io::{AsyncBufReadExt, BufStream},
    net::{TcpListener, UdpSocket},
    sync::mpsc::Sender,
    task::JoinHandle, time::timeout,
};

pub async fn setup(
    port: u16,
    tx: &Sender<Measurement>,
    #[cfg(feature = "signal_notifier")] signal_tx: Sender<String>,
) -> Vec<JoinHandle<()>> {
    let mut handles: Vec<JoinHandle<()>> = vec![];

    let mut addr_str = port.to_string();
    addr_str.insert_str(0, "0.0.0.0:");
    let listen_on: HostnameSocketAddr = HostnameSocketAddr::from_str(addr_str.as_str()).unwrap();

    handles.push(tokio::spawn(tcp_listener(
        listen_on.addr,
        tx.clone(),
        #[cfg(feature = "signal_notifier")]
        signal_tx,
    )));
    handles.push(tokio::spawn(udp_listener(listen_on.addr, tx.clone())));
    handles
}

pub async fn udp_listener(addr: SocketAddr, tx: Sender<Measurement>) {
    let socket = UdpSocket::bind(addr).await.unwrap();
    //let mut buffer = vec![0u8; 1024];
    // let mut buffer = Vec::with_capacity(1024);
    let mut buf = [0u8; 1024];
    loop {
        let (_read, _src) = match socket.recv_from(&mut buf).await {
            Ok(x) => x,
            Err(e) => {
                log_error(&format!("InfluxDB Relay: UDP connection failed: {e}"));
                continue;
            }
        };
        #[cfg(feature = "influxdb_relay_debugging")]
        log(&format!(
            "InfluxDB UDP relay read {_read} bytes from {_src}"
        ));

        // let x = CStr::from(&buf);
        // let x = CStr::from_bytes_with_nul(&buf);
        let nul_range_end = buf.iter().position(|&c| c == b'\0').unwrap_or(buf.len()); // default to length if no `\0` present
        match std::str::from_utf8(&buf[0..nul_range_end]) {
            Ok(str) => {
                for line in str.lines() {
                    forward_line(
                        line,
                        &tx,
                        #[cfg(feature = "track_source")]
                        MeasurementSource::UdpRelay(_src),
                    )
                    .await;
                }
            }
            Err(e) => log_error(&format!(
                "failed to parse received data as UTF8! error: {e}; data={buf:?}"
            )),
        }
        // #[cfg(feature = "influxdb_relay_debugging")]
        // log(&format!("{buf:?}"));
        // buffer = [0u8; 1024];
        buf.iter_mut().for_each(|x| *x = 0);
    }
}

pub async fn tcp_listener(
    addr: SocketAddr,
    tx: Sender<Measurement>,
    #[cfg(feature = "signal_notifier")] signal_tx: Sender<String>,
) {
    let listener = TcpListener::bind(addr).await.unwrap();

    loop {
        let (stream, addr) = match listener.accept().await {
            Err(e) => {
                log_error(&format!(
                    "influxdb tcp_listener incomming connection failed: {e}"
                ));
                continue;
            }
            Ok(x) => x,
        };
        let mut stream = BufStream::new(stream);

        #[cfg(feature = "signal_notifier")]
        signal_tx
            .try_send(format!("Relay TCP Connection established with: {addr}"))
            .ok();

        let tx_clone = tx.clone();
        #[cfg(feature = "signal_notifier")]
        let signal_tx_clone = signal_tx.clone();
        // do not block the main thread, spawn a new task
        tokio::spawn(async move {
            let mut line = String::new();
            loop {
                if let Some(msg) = match timeout(Duration::from_mins(10), stream.read_line(&mut line)).await {
                    Ok(Ok(0)) => {
                        Some(format!(
                            "influxdb_relay.tcip_listener: {addr}: connection closed/broken (=read 0 bytes)"
                        ))
                    }
                    Ok(Ok(_read)) => {
                        #[cfg(feature = "influxdb_relay_debugging")]
                        log(&format!(
                            "influxdb_relay.tcip_listener: read {_read} bytes from src {addr}: {line}"
                        ));
                        forward_line(
                            &line,
                            &tx_clone,
                            #[cfg(feature = "track_source")]
                            MeasurementSource::TcpRelay(addr),
                        )
                        .await;
                        line.clear();
                        None
                    }
                    Ok(Err(e)) => {
                        Some(format!(
                            "influxdb_relay.tcip_listener: failed to read from connected source {addr}: {e}"
                        ))
                    }
                    Err(e) => {
                        Some(format!(
                            "influxdb_relay.tcip_listener: connected source {addr} timed out: {e}"
                        ))
                    }
                } {
                    log_error(&msg);
                    #[cfg(feature = "signal_notifier")]
                    signal_tx_clone
                        .try_send(msg)
                        .ok();
                    break;
                }
            }
        });
    }
}

lazy_static! {
    static ref parser: Regex = Regex::new("^(,.+=.+)? ([^ ]+)=([-.0-9]+) ?([0-9]*)\n?$").unwrap();
}

async fn forward_line(
    line: &str,
    tx: &Sender<Measurement>,
    #[cfg(feature = "track_source")] src: MeasurementSource,
) {
    if line.is_empty() {
        log_error("received empty line, skipping!");
        return;
    }
    if line.trim().is_empty() {
        log_error(&format!(
            "received line only composed of {} whitespace characters, skipping!",
            line.len()
        ));
        return;
    }
    #[cfg(feature = "influxdb_relay_debugging")]
    log(&format!(
        "line length: {}; contains spaces: {}",
        line.len(),
        line.contains(' ')
    ));

    let Some(parsed) = parser.captures(line) else {
        log_error(&format!(
            "line can't be parsed with influxdb format regex: {line}"
        ));
        return;
    };
    #[cfg(feature = "influxdb_relay_debugging")]
    log(&format!("line parsed: {parsed:?}"));

    let nanos_now = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_nanos();

    let nanos_received = parsed
        .get(4)
        .map(|x| {
            if x.is_empty() {
                // don't make a fuss about lines without time, just assume they are from now
                return nanos_now;
            }
            x.as_str().parse::<u128>().unwrap_or_else(|e| {
                log_error(&format!(
                    "failed to parse time: {} ; error: {e}; line:\n{}",
                    x.as_str(),
                    line
                ));
                nanos_now
            })
        })
        .unwrap_or(nanos_now);
    let time = if nanos_received > nanos_now {
        #[cfg(feature = "influxdb_relay_debugging")]
        log(&format!("influxdb_relay: received line from the future, replacing time with current timestamp (now={}, received={})"
            ,format_now()
            ,format_nanos(nanos_received as _)
        ));
        nanos_now
    } else if Duration::from_nanos((nanos_now - nanos_received) as u64) > Duration::from_days(1) {
        log(&format!("influxdb_relay: received line with time more than a day in the past, using current timestamp instead (now={}, received={}) line: {line}"
            ,format_now()
            ,format_nanos(nanos_received as _)
        ));
        nanos_now
    } else {
        nanos_received
    };

    let Some(value_match) = parsed.get(3) else {
        log_error(&format!(
            "line is missing value when parsed with regex: {line}"
        ));
        return;
    };
    let value = match value_match.as_str().parse::<f64>() {
        Err(e) => {
            log_error(&format!(
                "influxdb_relay: value not parsable to f64! Error: {e}; value: {}; line:\n{line}",
                value_match.as_str()
            ));
            return;
        }
        Ok(f64) => f64,
    };
    let tags = parsed.get(1).map(|x| x.as_str()).unwrap_or("");
    let Some(metric) = parsed.get(2) else {
        log_error("influxdb_relay: regex bug");
        return;
    };
    tx.send(Measurement::new(
        tags.to_owned() + " " + metric.as_str(),
        value,
        time,
        #[cfg(feature = "track_source")]
        src,
    ))
    .await
    .unwrap_or_else(|e| log_error(&format!("failed to send into queue: {e}")))
}
