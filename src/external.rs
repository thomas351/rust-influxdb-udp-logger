use float_cmp::approx_eq;
use regex::Regex;
use std::{
    process::Command,
    str::{self, FromStr},
    time::Duration,
};
use tokio::{sync::mpsc::Sender, task::JoinHandle};

use crate::{
    metrics_sender::{metrics_sender, string_sender},
    Measurement,
};

#[derive(Debug, Clone)]
pub struct External {
    pub is_temperature_zone: bool,
    pub zone_or_prefix: String,
    pub devisor: f64,
    pub regex_unpack: String,
    pub query_frequency: f32,
    pub invalid_values: Vec<f64>,
    pub cmd: String,
    pub args: Vec<String>,
}

// https://doc.rust-lang.org/std/str/trait.FromStr.html
impl FromStr for External {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts: Vec<&str> = s.split(';').collect();

        let is_prefix: usize = (parts[0] == "OTHER").into();

        if parts.len() < (6 + is_prefix) {
            Err(format!("An external command definition in this format requires at least {} parts, see definition in help output.", (6+is_prefix)))
        } else {
            Ok(External {
                is_temperature_zone: (is_prefix == 0),
                zone_or_prefix: String::from(parts[is_prefix]),
                devisor: parts[1 + is_prefix].parse().unwrap_or(1.0),
                regex_unpack: String::from(parts[2 + is_prefix]),
                query_frequency: parts[3 + is_prefix].parse().unwrap_or(0.4),
                invalid_values: parts[4 + is_prefix]
                    .split('+')
                    .map(|s| s.parse::<f64>().unwrap_or(f64::NAN))
                    .filter(|v| !v.is_nan())
                    .collect(),
                cmd: String::from(parts[5 + is_prefix]),
                args: parts[6 + is_prefix..]
                    .iter()
                    .map(|x| String::from(*x))
                    .collect(),
            })
        }
    }
}

pub fn setup(
    externals: Vec<External>,
    hostname: &str,
    tx: &Sender<Measurement>,
) -> Vec<JoinHandle<()>> {
    #[cfg(feature = "external_debugging")]
    println!("external::setup started");
    let mut handles: Vec<JoinHandle<()>> = vec![];
    for ext in externals {
        #[cfg(feature = "external_debugging")]
        println!("external::setup processing: {ext:?}");
        let mut cmd: Command = Command::new(ext.cmd);
        cmd.args(ext.args);
        let unpacker: Result<Regex, regex::Error> =
            if ["", "(.*)"].contains(&ext.regex_unpack.as_str()) {
                Err(regex::Error::Syntax(String::from("skipping empty regex")))
            } else {
                Regex::new(&ext.regex_unpack)
            };
        if ext.invalid_values.is_empty() && approx_eq!(f64, ext.devisor, 1.0) && unpacker.is_err() {
            handles.push(tokio::spawn(string_sender(
                tx.clone(),
                if ext.is_temperature_zone {
                    format!(",zone={} {}.temperature", ext.zone_or_prefix, hostname)
                } else {
                    ext.zone_or_prefix.replace("{}", hostname)
                },
                Duration::from_secs_f32(ext.query_frequency),
                move || {
                    // https://doc.rust-lang.org/std/process/struct.Command.html#method.stdout
                    let result = cmd.output().unwrap();
                    str::from_utf8(&result.stdout).unwrap().into()
                },
            )));
        } else {
            handles.push(tokio::spawn(metrics_sender(
                tx.clone(),
                if ext.is_temperature_zone {
                    format!(",zone={} {}.temperature", ext.zone_or_prefix, hostname)
                } else {
                    ext.zone_or_prefix.replace("{}", hostname)
                },
                ext.invalid_values,
                Duration::from_secs_f32(ext.query_frequency),
                move || {
                    // https://doc.rust-lang.org/std/process/struct.Command.html#method.stdout
                    let result = cmd.output().unwrap();
                    let trimmed = str::from_utf8(&result.stdout).unwrap().trim();
                    let unpacked = match &unpacker {
                        Ok(regex) => {
                            let result = regex.captures(trimmed).unwrap();
                            result.get(1).unwrap().as_str()
                        }
                        Err(_) => trimmed,
                    };
                    unpacked.parse::<f64>().unwrap() / ext.devisor
                },
                #[cfg(feature = "track_source")]
                crate::MeasurementSource::ExternalCommand,
            )));
        }
    }
    #[cfg(feature = "external_debugging")]
    println!("external::setup finished");
    handles
}
