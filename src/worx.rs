#[cfg(feature = "track_source")]
use crate::MeasurementSource;
use crate::{log, log_error, Measurement};
use chrono::{DateTime, NaiveDateTime, TimeDelta};
use const_format::formatcp;
use reqwest::{RequestBuilder, Response};
use serde_json::{from_str, Value as JsonValue};
use std::{
    collections::HashMap,
    thread::sleep,
    time::{Duration, Instant, SystemTime, UNIX_EPOCH},
};
use tokio::sync::mpsc::Sender;

// adapted from https://github.com/MTrab/pyworxcloud/blob/master/pyworxcloud/api.py

// const BRAND_PREFIX: &str = "WX";
const ENDPOINT: &str = "api.worxlandroid.com";
const AUTH_ENDPOINT: &str = "id.worx.com";
const AUTH_URL: &str = formatcp!("https://{}/oauth/token", AUTH_ENDPOINT);
const AUTH_CLIENT_ID: &str = "150da4d2-bb44-433b-9429-3773adc70a2a";
const MOWER_URL: &str = formatcp!("https://{}/api/v2/product-items?status=1", ENDPOINT);

struct WorxHandler {
    tx: Sender<Measurement>,
    usr: String,
    pwd: String,
    token_type: Option<String>,
    access_token: Option<String>,
    refresh_token: Option<String>,
    expiration: Option<Instant>,
}

fn status_map(i: u8) -> &'static str {
    match i {
        0 => "Idle",
        1 => "Home",
        2 => "Start_sequence",
        3 => "Leaving_home",
        4 => "Follow_wire",
        5 => "Searching_home",
        6 => "Searching_wire",
        7 => "Mowing",
        8 => "Lifted",
        9 => "Trapped",
        10 => "Blade_blocked",
        11 => "Debug",
        12 => "Remote_control",
        30 => "Going_home",
        32 => "Cutting_edge",
        33 => "Searching_area",
        34 => "Pause",
        _ => "undefined",
    }
}

fn error_map(i: u8) -> &'static str {
    match i {
        0 => "None",
        1 => "Trapped",
        2 => "Lifted",
        3 => "Wire_missing",
        4 => "Outside_wire",
        5 => "Rain_delay",
        6 => "Close_door_to_mow",
        7 => "Close_door_to_go_home",
        8 => "Blade_motor_blocked",
        9 => "Wheel_motor_blocked",
        10 => "Trapped_timeout",
        11 => "Upside_down",
        12 => "Battery_low",
        13 => "Reverse_wire",
        14 => "Charge_error",
        15 => "Timeout_finding_home",
        _ => "undefined",
    }
}

fn json_get_str(obj: &JsonValue, key: &str) -> Result<String, String> {
    let Some(val) = obj.get(key) else {
        return Err(format!("worx json: didn't contain {key}!"));
    };
    match val.as_str() {
        Some(str) => Ok(str.to_owned()),
        None => Err(format!("worx json: {key} did not contain a String!")),
    }
}

fn json_get_i64(obj: &JsonValue, key: &str) -> Result<i64, String> {
    let Some(val) = obj.get(key) else {
        return Err(format!("worx json: didn't contain {key}!"));
    };
    match val.as_i64() {
        Some(num) => Ok(num),
        None => Err(format!("worx json: {key} did not contain a Number!")),
    }
}

fn json_get_f64(obj: &JsonValue, key: &str) -> Result<f64, String> {
    let Some(val) = obj.get(key) else {
        return Err(format!("worx json: didn't contain {key}!"));
    };
    match val.as_f64() {
        Some(num) => Ok(num),
        None => Err(format!("worx json: {key} did not contain a Number!")),
    }
}

fn json_get_bool(obj: &JsonValue, key: &str) -> Result<bool, String> {
    let Some(val) = obj.get(key) else {
        return Err(format!("worx json: didn't contain {key}!"));
    };
    match val.as_bool() {
        Some(bit) => Ok(bit),
        None => Err(format!("worx json: {key} did not contain a boolean!")),
    }
}

impl WorxHandler {
    fn new(usr: String, pwd: String, tx: Sender<Measurement>) -> Self {
        WorxHandler {
            tx,
            usr,
            pwd,
            token_type: None,
            access_token: None,
            refresh_token: None,
            expiration: None,
        }
    }

    async fn authenticate(&mut self) {
        if self.access_token.is_some()
            && self.token_type.is_some()
            && let Some(expiration) = &self.expiration
            && *expiration > Instant::now()
        {
            return;
        }
        let mut params: HashMap<&str, &str> = HashMap::new();
        params.insert("client_id", AUTH_CLIENT_ID);
        params.insert("scope", "*");
        if let Some(refresh_token) = &self.refresh_token {
            params.insert("grant_type", "refresh_token");
            params.insert("refresh_token", refresh_token);
        } else {
            params.insert("grant_type", "password");
            params.insert("username", &self.usr);
            params.insert("password", &self.pwd);
        }

        let req = self
            .add_headers(reqwest::Client::new().post(AUTH_URL))
            .form(&params);
        match req.send().await {
            Ok(response) => {
                self.store_tokens(response).await;
            }
            Err(e) => log_error(&format!("worx auth request failed: {e}")),
        }
    }

    async fn store_tokens(&mut self, response: Response) {
        let txt = response.text().await.unwrap_or_else(|e| format!("{e}"));
        #[cfg(feature = "worx_debugging")]
        log(&format!("worx auth response: {txt}"));
        match from_str::<JsonValue>(&txt) {
            Ok(json) => {
                match json_get_str(&json, "access_token") {
                    Ok(t) => self.access_token = Some(t),
                    Err(e) => log_error(&e),
                };
                match json_get_str(&json, "refresh_token") {
                    Ok(t) => self.refresh_token = Some(t),
                    Err(e) => log_error(&e),
                };
                match json_get_str(&json, "token_type") {
                    Ok(t) => self.token_type = Some(t),
                    Err(e) => log_error(&e),
                };
                match json_get_i64(&json, "expires_in") {
                    Ok(n) => {
                        self.expiration = Instant::now().checked_add(Duration::from_secs(n as _))
                    }
                    Err(e) => log_error(&e),
                }
            }
            Err(e) => log_error(&format!(
                "worx auth response not parsable as json: {txt}; Error: {e}"
            )),
        }
    }

    fn add_headers(&self, mut builder: RequestBuilder) -> RequestBuilder {
        if let Some(access_token) = &self.access_token
            && let Some(token_type) = &self.token_type
            && let Some(expiration) = &self.expiration
            && Instant::now() < *expiration
        {
            builder = builder.header("Authorization", format!("{token_type} {access_token}"));
        } else {
            builder = builder.header("Content-Type", "application/x-www-form-urlencoded");
        }
        builder.header("Accept", "application/json")
    }

    async fn send_state(
        &mut self,
        state: JsonValue,
        last_status_ts: Option<DateTime<chrono::Local>>,
    ) -> Option<DateTime<chrono::Local>> {
        let Some(mower) = state.get(0) else {
            log_error("worx state json did not contain a mower!");
            return None;
        };
        match json_get_bool(mower, "online") {
            Ok(v) => {
                self.tx
                    .send(Measurement::new(
                        " worx.online".into(),
                        v.into(),
                        SystemTime::now()
                            .duration_since(UNIX_EPOCH)
                            .unwrap()
                            .as_nanos(),
                        #[cfg(feature = "track_source")]
                        MeasurementSource::Worx,
                    ))
                    .await
            }
            Err(e) => {
                log_error(&e);
                Ok(())
            }
        }
        .ok();
        let Some(s) = mower.get("last_status") else {
            log_error("worx mower0 is missing 'last_status' object!");
            return None;
        };
        let status_ts = match json_get_str(s, "timestamp") {
            Ok(t) => match NaiveDateTime::parse_from_str(&t, "%Y-%m-%d %H:%M:%S") {
                Ok(ts) => DateTime::from(ts.and_utc()),
                Err(e) => {
                    log_error(&format!(
                        "worx status timestamp unparseable: {t}; error={e}"
                    ));
                    return None;
                }
            },
            Err(e) => {
                log_error(&e);
                return None;
            }
        };
        if let Some(last_status_ts1) = last_status_ts
            && status_ts == last_status_ts1
        {
            #[cfg(feature = "worx_debugging")]
            log(&format!(
                "worx status has same timestamp as last time = {status_ts}, won't send it again."
            ));
            return last_status_ts;
        }
        let Some(status_nanos) = status_ts.timestamp_nanos_opt() else {
            log_error(&format!(
                "worx status timestamp out of ~584 years range around 1970! {status_ts:?}"
            ));
            return Some(status_ts);
        };
        let Some(pl) = s.get("payload") else {
            log_error("worx status is missing payload object!");
            return Some(status_ts);
        };
        let Some(dat) = pl.get("dat") else {
            log_error("worx payload is missing dat object!");
            return Some(status_ts);
        };
        self.send_metric(dat, "rsi", " worx.rssi".into(), status_nanos as _)
            .await;
        let status = match json_get_i64(dat, "ls") {
            Ok(i) => status_map(i as _),
            Err(e) => {
                log_error(&e);
                return Some(status_ts);
            }
        };
        let error = match json_get_i64(dat, "le") {
            Ok(i) => error_map(i as _),
            Err(e) => {
                log_error(&e);
                return Some(status_ts);
            }
        };

        let Some(bt) = dat.get("bt") else {
            log_error("worx payload.dat is missing bt object!");
            return Some(status_ts);
        };
        self.send_metric(bt, "t", " worx.temperature".into(), status_nanos as _)
            .await;
        self.send_metric(bt, "v", " worx.voltage".into(), status_nanos as _)
            .await;
        self.send_metric(
            bt,
            "p",
            format!(",status={status},error={error} worx.charge"),
            status_nanos as _,
        )
        .await;
        self.send_metric(bt, "nr", " worx.cycles".into(), status_nanos as _)
            .await;
        self.send_metric(bt, "c", " worx.charging".into(), status_nanos as _)
            .await;

        let Some(st) = dat.get("st") else {
            log_error("worx payload.dat is missing st object!");
            return Some(status_ts);
        };
        self.send_metric(st, "b", " worx.blade_time_mins".into(), status_nanos as _)
            .await;
        self.send_metric(st, "d", " worx.driven_distance_m".into(), status_nanos as _)
            .await;
        self.send_metric(st, "wt", " worx.mowing_mins".into(), status_nanos as _)
            .await;
        Some(status_ts)
    }

    async fn send_metric(&self, obj: &JsonValue, key: &str, prefix: String, time: u128) {
        match json_get_f64(obj, key) {
            Ok(value) => {
                self.tx
                    .send(Measurement::new(
                        prefix,
                        value,
                        time,
                        #[cfg(feature = "track_source")]
                        MeasurementSource::Worx,
                    ))
                    .await
            }
            Err(e) => {
                log_error(&e);
                Ok(())
            }
        }
        .ok();
    }

    async fn query_state(
        &mut self,
        last_status_ts: Option<DateTime<chrono::Local>>,
    ) -> Option<DateTime<chrono::Local>> {
        self.authenticate().await;
        let req = self.add_headers(reqwest::Client::new().get(MOWER_URL));
        #[cfg(feature = "worx_debugging")]
        log(&format!("worx get_mowers request: {req:?}"));
        match req.send().await {
            Ok(resp) => {
                #[cfg(feature = "worx_debugging")]
                log(&format!("worx get_mowers response: {resp:?}"));
                let txt = resp.text().await.unwrap_or_else(|e| format!("{e}"));
                #[cfg(feature = "worx_debugging")]
                log(&format!("worx get_mowers response text: {txt}"));
                match from_str::<JsonValue>(&txt) {
                    Ok(json) => self.send_state(json, last_status_ts).await,
                    Err(e) => {
                        log_error(&format!(
                            "worx api response not parsable as json: {txt}; Error: {e}"
                        ));
                        None
                    }
                }
            }
            Err(e) => {
                log_error(&format!("worx get_mowers failed: {e}"));
                None
            }
        }
    }
}

pub async fn http_client_loop(
    usr: String,
    pwd: String,
    tx: Sender<Measurement>,
    frequency: TimeDelta,
) {
    log("worx: start");
    let mut last_status_ts: Option<DateTime<chrono::Local>> = None;
    let mut handler = WorxHandler::new(usr, pwd, tx);
    loop {
        last_status_ts = handler.query_state(last_status_ts).await;
        let wait = match last_status_ts {
            Some(last_status_ts) => {
                let now = chrono::Local::now();
                let next = last_status_ts.checked_add_signed(frequency).unwrap();
                if now > next {
                    #[cfg(feature = "worx_debugging")]
                    log(&format!("worx robot is offline, check again in 11 minutes"));
                    Duration::from_mins(11)
                } else {
                    let time_to_sleep = next.signed_duration_since(now);
                    #[cfg(feature = "worx_debugging")]
                    log(&format!("last_status={last_status_ts} ; now={now} ; next_status={next} ; time_to_sleep={time_to_sleep}"));
                    match time_to_sleep.to_std() {
                        Ok(d) => d,
                        Err(e) => {
                            log_error(&format!(
                                "worx: failed to convert timedelta to duration: {e}"
                            ));
                            Duration::from_mins(11)
                        }
                    }
                }
            }
            None => Duration::from_mins(11),
        };
        sleep(wait);
    }
    // log_error("worx http_client_loop ended!?");
}
