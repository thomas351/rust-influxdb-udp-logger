use std::{
    collections::HashMap,
    ffi::OsStr,
    time::{Duration, SystemTime, UNIX_EPOCH},
};
use sysinfo::{Components, Disks, System};
use tokio::sync::mpsc::Sender;
use tokio::time::sleep;

use crate::{log_error, Load, Measurement};

pub async fn watcher(
    hostname: String,
    tx: Sender<Measurement>,
    skip_zones: Vec<String>,
    skip_all_zones: bool,
    zone_devisor: f32,
    load: Load,
) {
    let mut disks = Disks::new();
    let mut components = Components::new();
    loop {
        if !skip_all_zones {
            send_component_temperatures(&mut components, &hostname, &tx, &skip_zones, zone_devisor)
                .await;
        }
        send_diskspace(&mut disks, &hostname, &tx).await;
        if load != Load::None {
            send_sysload(&hostname, &tx, load).await;
        }
        sleep(Duration::from_millis(400)).await;
    }
}

async fn send_component_temperatures(
    components: &mut Components,
    hostname: &str,
    tx: &Sender<Measurement>,
    skip_zones: &[String],
    zone_devisor: f32,
) {
    let time = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_nanos();
    components.refresh_list();
    for component in components {
        let zone = component.label().replace(' ', "_");
        if !skip_zones.contains(&zone) {
            let prefix = format!(",zone={zone} {hostname}.temperature");
            let m = Measurement::new(
                prefix,
                (component.temperature() / zone_devisor) as _,
                time,
                #[cfg(feature = "track_source")]
                crate::MeasurementSource::Sysinfo,
            );
            tx.send(m).await.ok();
        }
    }
}

async fn send_sysload(hostname: &str, tx: &Sender<Measurement>, selector: Load) {
    let time = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_nanos();
    let load_avg = System::load_average(); // this loads data live from system, not stored in sys struct
    if selector == Load::All {
        for (window, value) in [
            ("1m", load_avg.one),
            ("5m", load_avg.five),
            ("15m", load_avg.fifteen),
        ] {
            let prefix = format!(",window={window} {hostname}.sysload");
            tx.send(Measurement::new(
                prefix,
                value,
                time,
                #[cfg(feature = "track_source")]
                crate::MeasurementSource::Sysinfo,
            ))
            .await
            .ok();
        }
    } else {
        let prefix = format!(" {hostname}.sysload");
        let m = match selector {
            Load::OneMinute => Measurement::new(
                prefix,
                load_avg.one,
                time,
                #[cfg(feature = "track_source")]
                crate::MeasurementSource::Sysinfo,
            ),
            Load::FiveMinutes => Measurement::new(
                prefix,
                load_avg.five,
                time,
                #[cfg(feature = "track_source")]
                crate::MeasurementSource::Sysinfo,
            ),
            Load::FifteenMinutes => Measurement::new(
                prefix,
                load_avg.fifteen,
                time,
                #[cfg(feature = "track_source")]
                crate::MeasurementSource::Sysinfo,
            ),
            _ => {
                log_error("sysinfo.send_sysload: this line should never be reached");
                return;
            }
        };
        tx.send(m).await.ok();
    }
}

async fn send_diskspace(disks: &mut Disks, hostname: &str, tx: &Sender<Measurement>) {
    let time = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_nanos();

    disks.refresh_list();
    // sys.refresh_disks_list();
    // systems using btrfs subvolumes might have multiple mountpoints for single diskpartition, only report the "top" mount for each diskpartition
    let mut top_mounts: HashMap<&OsStr, &OsStr> = HashMap::new();
    let mut usages: HashMap<&OsStr, f64> = HashMap::new();
    for disk in disks {
        match top_mounts.get(&disk.name()) {
            Some(other) => {
                if other.len() > disk.mount_point().as_os_str().len() {
                    top_mounts.insert(disk.name(), disk.mount_point().as_os_str());
                }
            }
            None => {
                top_mounts.insert(disk.name(), disk.mount_point().as_os_str());
                usages.insert(
                    disk.name(),
                    disk.available_space() as f64 / disk.total_space() as f64,
                );
            }
        };
    }
    for (device, mount) in top_mounts {
        let space = *usages.get(device).unwrap();
        let id = mount.to_string_lossy();
        let prefix = format!(",mount={id} {hostname}.diskspace");
        let m = Measurement::new(
            prefix,
            space,
            time,
            #[cfg(feature = "track_source")]
            crate::MeasurementSource::Sysinfo,
        );
        tx.send(m).await.ok();
    }
}
