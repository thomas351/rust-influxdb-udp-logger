// copied inital version from:
// https://dev.to/geoffreycopin/-build-a-web-server-with-rust-and-tokio-part-0-the-simplest-possible-get-handler-1lhi
#[cfg(feature = "alias")]
use crate::apply_aliases_opt;
#[cfg(feature = "http_server_debugging")]
use crate::log;
use crate::{
    data_sender::{MeasurementCache, SenderCache},
    format_nanos,
    human_readable_duration::format_duration,
    log_error, HostnameSocketAddr,
};
use itertools::Itertools;
use std::{
    collections::HashMap,
    str::FromStr,
    sync::Arc,
    time::{Duration, Instant, SystemTime, UNIX_EPOCH},
};
use tokio::{
    io::{self, AsyncBufReadExt, AsyncWriteExt, BufStream},
    net::TcpListener,
    sync::{Mutex, RwLock},
};

const HTTP_PREFIX: &str = "HTTP/1.1 200 OK

<html>
<head><title>Rust InfluxDB Logger Status</title>";
const HTTP_REFRESH_PREFIX: &str =
    "<meta http-equiv=\"refresh\" content=\"1\"></head><body><a href=\"../\">stop refreshing</a><br>";
const HTTP_NO_REFRESH_PREFIX: &str =
    "</head><body><a href=\"./refresh/\">enable auto-refresh</a><br>";
#[cfg(feature = "track_source")]
const HTTP_TABLE_HEADER: &str =
    "<br><table><tr><th>metric</th><th>tags</th><th>value</th><th>last sent</th><th>measured</th><th>source</th></tr>
";
#[cfg(not(feature = "track_source"))]
const HTTP_TABLE_HEADER: &str =
    "<br><table><tr><th>metric</th><th>tags</th><th>value</th><th>last sent</th><th>measured</th></tr>
";
const HTTP_SUFFIX: &str = "</table></body></html>";

// fn cmp_prefix(a: &str, b: &str) -> Ordering {
//     let (tags_a, metric_a) = a.split_at(a.rfind(' ').unwrap());
//     let (tags_b, metric_b) = b.split_at(b.rfind(' ').unwrap());
//     (metric_a.to_owned() + tags_a).cmp(&(metric_b.to_owned() + tags_b))
// }

struct HttpCache {
    src: Arc<RwLock<SenderCache>>,
    absolute: Option<(String, Instant)>,
    relative: Option<(String, Instant)>,
}

impl HttpCache {
    fn new(src: Arc<RwLock<SenderCache>>) -> Self {
        HttpCache {
            src,
            absolute: None,
            relative: None,
        }
    }

    async fn refresh_if_time_and_idle(&mut self, absolute: bool) {
        let mode = if absolute {
            &self.absolute
        } else {
            &self.relative
        };
        let refresh = if let Some((_value, instant)) = mode {
            if instant.elapsed() > Duration::from_secs(1) && self.src.read().await.sender_idle {
                // last cache is more than a second old and sender is currently idle, let's rebuild!
                true
            } else {
                // cache is less than a second old, or the sender is currently busy processing measurements. let's reuse the cache for this request
                false
            }
        } else {
            // this mode has no html cache yet, need to refresh it otherwise we have nothing to send!
            true
        };

        if refresh {
            let fresh = (build(&self.src, absolute).await, Instant::now());
            if absolute {
                self.absolute = Some(fresh);
            } else {
                self.relative = Some(fresh);
            }
        }
    }

    async fn html_output(
        &mut self,
        absolute: bool,
        refresh: bool,
        stream: &mut BufStream<tokio::net::TcpStream>,
    ) -> io::Result<()> {
        self.refresh_if_time_and_idle(absolute).await;
        let print = if absolute {
            self.absolute.as_ref().unwrap().0.as_str()
        } else {
            self.relative.as_ref().unwrap().0.as_str()
        };

        stream.write_all(HTTP_PREFIX.as_bytes()).await?;
        stream
            .write_all(
                if refresh {
                    HTTP_REFRESH_PREFIX
                } else {
                    HTTP_NO_REFRESH_PREFIX
                }
                .as_bytes(),
            )
            .await?;
        stream.write_all(print.as_bytes()).await?;
        stream.flush().await?;
        Ok(())
    }
}

fn reverse_tags_and_metrics(prefix: &str) -> String {
    if let Some((tags, metric)) = prefix.rsplit_once(' ') {
        metric.to_owned() + tags
    } else {
        prefix.to_owned()
    }
}

fn build_row(
    html: &mut String,
    prefix: &str,
    c: &MeasurementCache,
    absolute: bool,
    current_nanos: u128,
) {
    let sent = time(absolute, c.last_sent, current_nanos);
    let measured = time(absolute, c.last_measured, current_nanos);
    let (tags, metric) = prefix.split_at(prefix.rfind(' ').unwrap());
    #[cfg(feature = "track_source")]
        html.push_str(&format!(
            "<tr><td>{}</td><td>{tags}</td><td>{}</td><td>{sent}</td><td>{measured}</td><td>{}</td></tr>\r\n",
            &metric[1..],
            c.value,
            c.src,
        ));
    #[cfg(not(feature = "track_source"))]
    html.push_str(&format!(
        "<tr><td>{}</td><td>{tags}</td><td>{}</td><td>{sent}</td><td>{measured}</td></tr>\r\n",
        &metric[1..],
        c.value,
    ));
}

async fn build(cache: &RwLock<SenderCache>, absolute: bool) -> String {
    let mut html = crate::format_now() + HTTP_TABLE_HEADER;
    let current_nanos = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_nanos();
    html.insert_str(0, "generated ");

    let read_lock = cache.read().await;

    let mut aliased = HashMap::new();
    let mut others = Vec::new();
    for prefix in read_lock.measurements.keys() {
        #[cfg(feature = "alias")]
        let alias_opt = apply_aliases_opt(prefix, &read_lock.prefix_aliases);
        #[cfg(not(feature = "alias"))]
        let alias_opt: Option<&str> = None;
        if let Some(changed) = alias_opt {
            aliased.insert(prefix, changed);
        } else {
            others.push(prefix);
        }
    }

    for (prefix, alias) in aliased
        .iter()
        .sorted_by_key(|(_k, v)| reverse_tags_and_metrics(v))
    {
        build_row(
            &mut html,
            alias,
            read_lock.measurements.get(*prefix).unwrap(),
            absolute,
            current_nanos,
        );
    }
    for prefix in others.iter().sorted_by_key(|k| reverse_tags_and_metrics(k)) {
        build_row(
            &mut html,
            prefix,
            read_lock.measurements.get(*prefix).unwrap(),
            absolute,
            current_nanos,
        );
    }
    html.push_str(HTTP_SUFFIX);
    html
}

fn time(absolute: bool, src_nanos: u128, current_nanos: u128) -> String {
    if absolute {
        format_nanos(src_nanos as _)
    } else {
        let elapsed = Duration::from_nanos((current_nanos - src_nanos) as _);
        format_duration(elapsed).to_string()
    }
}

pub async fn listener(port: u16, src: Arc<RwLock<SenderCache>>) -> anyhow::Result<()> {
    let mut addr_str = port.to_string();
    addr_str.insert_str(0, "0.0.0.0:");
    let listen_on: HostnameSocketAddr = HostnameSocketAddr::from_str(addr_str.as_str()).unwrap();
    let listener = TcpListener::bind(listen_on.addr).await.unwrap();

    #[cfg(feature = "http_server_debugging")]
    log(&format!("listening on: {}", listener.local_addr()?));

    let html_cache = Arc::new(Mutex::new(HttpCache::new(src)));

    loop {
        let (stream, _addr) = listener.accept().await?;
        let mut stream = BufStream::new(stream);

        let html_cache2 = html_cache.clone();
        // do not block the main thread, spawn a new task
        tokio::spawn(async move {
            #[cfg(feature = "http_server_debugging")]
            log(&format!("new connection: {}", _addr));

            let mut line = String::new();
            let mut refresh: bool = false;
            let mut absolute: bool = false;
            loop {
                match stream.read_line(&mut line).await {
                    Ok(0) => {
                        #[cfg(feature = "http_server_debugging")]
                        log("http_server request: reached EOF = Ok(0)");
                        break;
                    }
                    Ok(_length) => {
                        if line.contains("refresh") {
                            refresh = true;
                        }
                        if line.contains("absolute") {
                            absolute = true;
                        }
                        #[cfg(feature = "http_server_debugging")]
                        log(&format!(
                            "http_server request line of length {_length}:\n{line}"
                        ));
                    }
                    Err(e) => log_error(&format!("http_server request read_line: Error = {e}")),
                }

                if line.is_empty() || line == "\n" || line == "\r\n" {
                    #[cfg(feature = "http_server_debugging")]
                    log("http_server request read empty line, consider request complete, send reply now");
                    break;
                }
                line.clear();
            }
            match html_cache2
                .lock()
                .await
                .html_output(absolute, refresh, &mut stream)
                .await
            {
                Ok(_) =>
                {
                    #[cfg(feature = "http_server_debugging")]
                    log("http_server write response succeeded")
                }
                Err(e) => log_error(&format!("http_server: response failed to send: {e}")),
            }
        });
    }
}
