use binread::{io::Cursor, BinRead, BinReaderExt};
#[cfg(feature = "fronius_debugging")]
use chrono::TimeDelta;
use chrono::{DateTime, FixedOffset};
use lazy_static::lazy_static;
use std::{
    borrow::Cow::{self, Borrowed, Owned},
    net::SocketAddr,
    time::{Duration, SystemTime, UNIX_EPOCH},
};
use tokio::sync::mpsc::Sender;
use tokio::time::sleep;
use tokio_modbus::{
    client::{Context, Reader},
    slave::SlaveContext,
    Slave,
};

use crate::Measurement;

// not commented out variables = 60 registers = 120 bytes long, from register 40071 to 40130 (40132-40071=59)
#[derive(BinRead, Debug)]
struct CommonBlock {
    // _sid: [char; 4], // = SunS
    // _id: u16, // = 1
    // _model_block_length: u16, // = 65
    // _manifacturer: [char; 32], // = Fronius
    // _model: [char; 32], // = Symo GEN24 6.0 Plus
    // _firmware_version: [char; 16], // = empty
    // _software_version: [char; 16], // = 1.28.7-1
    // _serial_number: [char; 32],
    // _device_address: u16, // always = 1?
    // _phase_count_plus_110: u16, // = 113
    // _inverter_bock_length: u16, // = 60
    // above = registers 40000 to 40070 -> don't need to read those, static stuff.

    //start reading at register address 40071:
    ac_current_ampere_total: f32,
    ac_current_ampere_phase_a: f32,
    ac_current_ampere_phase_b: f32,
    ac_current_ampere_phase_c: f32,
    ac_volt_phase_ab: f32,
    ac_volt_phase_bc: f32,
    ac_volt_phase_ca: f32,
    ac_volt_phase_a: f32,
    ac_volt_phase_b: f32,
    ac_volt_phase_c: f32,
    ac_power_watt: f32,
    ac_frequency_hz: f32,
    apparent_power_va: f32,
    reactive_power_var: f32,
    power_factor_pct: f32,
    ac_lifetime_production: f32,
    _dc_ampere: [u8; 4], //f32, //unsupported, always = 7FC00000 //
    _dc_volt: [u8; 4],   //f32, //unsupported, always = 7FC00000 //
    dc_watt: f32,
    temperature_cabinet_degc: f32,
    _temperature_heat_sink_degc: [u8; 4], //f32, //unsupported, always = 7FC00000 //
    _temperature_transformer_degc: [u8; 4], //f32, //unsupported, always = 7FC00000 //
    operation_state: u16,
    operation_state_vendor: u16,
    event_flags: u64,
    event_flags_vendor: u128, //should be only 128 bits = 16 bytes, don't know why we have an extra 4 bytes before well known 120 value below..?

                              // none of the fields below seem to contain any relevant non-static information:
                              // _id_nameplate: u16, // = 120
                              // _nameplate_block_length: u16, // = 26
                              // _der_device_type: u16, // = 4 to indicate PV device
                              // _capability_watt: u16,
                              // _capability_watt_scale: i16,
                              // _capability_voltampere: u16,
                              // _capability_voltampere_scale: i16,
                              // _capability_var_quadrant1: i16,
                              // _capability_var_quadrant2: i16,
                              // _capability_var_quadrant3: i16,
                              // _capability_var_quadrant4: i16,
                              // _capability_var_scale: i16,
                              // _ac_max_rms: u16,
                              // _ac_max_rms_scale: i16,
                              // _capability_power_factor_ccos_quadrant1: i16,
                              // _capability_power_factor_ccos_quadrant2: i16,
                              // _capability_power_factor_ccos_quadrant3: i16,
                              // _capability_power_factor_ccos_quadrant4: i16,
                              // _capability_power_factor_ccos_scale: i16,
                              // _storage_energy_nominal_wh: u16,
                              // _storage_energy_nominal_scale: i16,
                              // _storage_energy_usable: u16,
                              // _storage_energy_usable_scale: i16,
                              // _max_charge_rate_watt: u16,
                              // _max_charge_rate_scale: i16,
                              // _max_discharge_rate_watt: u16,
                              // _max_discharge_rate_scale: i16,

                              // _pad_register: [u8;2], // u16,

                              // _id_basic_settings: u16, // = 121
                              // _basic_settings_block_length: u16, // = 30
                              // _max_power_set_watt: u16,
                              // _pcc_ref_volt: u16,
                              // _pcc_ref_offset: i16,
                              // _volt_set_max: u16, // not supported
                              // _volt_set_min: u16, // not supported
                              // _apparent_power_set_max_va: u16,
                              // _reactive_power_var_set_quadrant1: i16,
                              // _reactive_power_var_set_quadrant2: i16,
                              // _reactive_power_var_set_quadrant3: i16,
                              // _reactive_power_var_set_quadrant4: i16,
                              // _ramp_rate_of_active_power_change: u16,
                              // _power_factor_ccos_set_quadrant1: i16,
                              // _power_factor_ccos_set_quadrant2: i16,
                              // _power_factor_ccos_set_quadrant3: i16,
                              // _power_factor_ccos_set_quadrant4: i16,
                              // _var_action_characterization: u16, // VAR action on change between charging and discharging: 1=switch 2=maintain VAR characterization.
                              // _apparent_power_calculation_method: u16, // Calculation method for total apparent power. 1=vector 2=arithmetic.
                              // _ramp_rate_set_pct: u16, // Setpoint for maximum ramp rate as percentage of nominal maximum ramp rate. This setting will limit the rate that watts delivery to the grid can increase or decrease in response to intermittent PV generation.
                              // _nominal_frequency_set_hz: u16, // Setpoint for nominal frequency at the ECP.
                              // _single_phase_used: u16, // Identity of connected phase for single phase inverters. A=1 B=2 C=3.
                              // _max_power_set_scale: i16,
                              // _pcc_ref_scale: i16,
                              // _pcc_ref_offset_scale: i16,
                              // _volt_set_min_max_scale: i16,
                              // _apparent_power_set_max_va_scale: i16,
                              // _reactive_power_var_set_scale: i16,
                              // _ramp_rate_of_active_power_scale: i16,
                              // _power_factor_ccos_set_scale: i16,
                              // _ramp_rate_set_scale: i16,
                              // _nominal_frequency_set_scale: i16,
}

// not commented out variables = 68 registers = 136 bytes long, from register 40193 to 40262 (40262-40193=69)
#[derive(BinRead, Debug)]
struct StatusBlock {
    // _id_measurement_status: u16, // = 122
    // _measurement_status_block_length: u16, // = 44

    // start reading at register address = 40193:
    pv_inverter_status: u16, // 0: Connected, 1: Available, 2: Operating, 3: Test
    storage_inverter_status: u16, // 0: Connected, 1: Available, 2: Operating, 3: Test
    ecp_connection_status: u16, // disconnected=0  connected=1
    ac_lifetime_active_real_energy_output_wh: u64,
    _ac_lifetime_apparent_energy_output_vah: u64, // Not supported
    _ac_lifetime_reactive_energy_output_q1_varh: u64, // Not supported
    _ac_lifetime_reactive_energy_output_q2_varh: u64, // Not supported
    _ac_lifetime_reactive_energy_output_q3_varh: u64, // Not supported
    _ac_lifetime_reactive_energy_output_q4_varh: u64, // Not supported
    _vars_amount_available: i16,                  // Not supported
    _vars_amount_scale: i16,                      // Not supported
    _watt_amount_available: i16,                  // Not supported
    _watt_amount_scale: i16,                      // Not supported
    setpoint_limits_reached_bitmask: u32, // [u8;4], // Bit Mask indicating setpoint limit(s) reached. Bits are persistent and must be cleared by the controller. Not supported
    active_inverter_controls_bitmask: u32, //[u8;4], // Bit Mask indicating which inverter controls are currently active. Bit 0: FixedW,  Bit 1: FixedVAR, Bit 2: FixedPF
    _time_sync_source: [char; 4],          // Source of time synchronization = RTC
    _seconds_since_y2k: u64,               // Seconds since 01-01-2000 00:00 UTC = 0?!
    voltage_ride_through_modes_active: u16, // Bit Mask indicating which voltage ride through modes are currently active. Not supported
    isolation_resistance: u16,              // Not supported
    isolation_resistance_scale: i16,        // Scale factor for Isolation resistance; Not supported

                                            // _padding: [u8;4],

                                            // _id_immediate_controls: u16, // A well-known value 123.  Uniquely identifies this as a SunSpec Immediate Controls Model
                                            // _immediate_controls_block_length: u16, // Length of Immediate Controls Model
                                            // _connection_time_window_secs: u16, // Time window for connect/disconnect.
                                            // _connection_timeout_period_secs: u16, // Timeout period for connect/disconnect.
                                            // _connection_control: u16, // Connection control. 0: Disconnected, 1: Connected
                                            // _power_output_limit_set_watt_pct: u16, // Set power output to specified level.
                                            // _power_output_limit_time_window_secs: u16, // Time window for power limit change. 0-300
                                            // _power_output_limit_timeout_secs: u16, // Timeout period for power limit. 0-28800
                                            // _power_output_limit_ramp: u16, // Ramp time for moving from current setpoint to new setpoint. 0 - 65534 (0xFFFF has the same effect as 0x0000)
                                            // _power_output_limit_set_enabled_flag: u16, // Throttle enable/disable control. 0: Disabled, 1: Enabled
                                            // _power_factor_output_set_cos: i16, // Set power factor to specific value - cosine of angle. 0.8 to 1.0 and -0.8 to -0.999
                                            // _power_factor_output_time_window_secs: u16, // Time window for power factor change. 0-300
                                            // _power_factor_output_timeout_secs: u16, // Timeout period for power factor. 0-28800
                                            // _power_factor_output_ramp: u16, // Ramp time for moving from current setpoint to new setpoint.
                                            // _power_factor_output_set_enabled_flag: u16, // Enumerated valued.  Fixed power factor enable/disable control.
                                            // _reactive_power_wmax_pct: i16, // Reactive power in percent of I_WMax.
                                            // _reactive_power_varmax_pct: i16, // Reactive power in percent of I_VArMax.
                                            // _reactive_power_varaval_pct: i16, // Reactive power in percent of I_VArAval.
                                            // _var_pct_time_window_secs: u16, // Time window for VAR limit change.
                                            // _var_pct_timeout_secs: u16, // Timeout period for VAR limit.
                                            // _var_pct_ramp: u16, // Ramp time for moving from current setpoint to new setpoint.
                                            // _var_pct_mode: u16, // Enumerated value. VAR limit mode.
                                            // _var_pct_set_enabled_flag: u16, // Enumerated valued.  Fixed VAR enable/disable control.
                                            // _power_output_limit_scale: i16, // Scale factor for power output percent.
                                            // _power_factor_output_scale: i16, // Scale factor for power factor.
                                            // _reactive_power_scale: i16, // Scale factor for reactive power.

                                            // _pad_register: [u8;4], // u16,
}

// not commented out variables = 112 registers =  bytes long, from register 40265 to 40378 (40378-40265=113)
#[derive(BinRead, Debug)]
struct MpptBlock {
    // _id_inverter_extension: u16, // A well-known value 160.  Uniquely identifies this as a SunSpec Multiple MPPT Inverter Extension Model Mode
    // _inverter_extension_block_length: u16, // Length of Multiple MPPT Inverter Extension Model = 88
    current_scale: i16,     // Current Scale Factor
    voltage_scale: i16,     // Voltage Scale Factor
    power_scale: i16,       // Power Scale Factor
    energy_scale: i16,      // Energy Scale Factor
    global_events: u32,     //[u8; 4], // Global Events
    _modules_number: u16, // Number of Modules; number of mppt trackers + 2 * number of battery inputs ; 2 MPPT and 1 BATTERY => 4 repeating blocks
    _timestamp_period: u16, // Timestamp Period (not supported)
    dc: [DcBlock; 4],
    // _dc1: DcBlock, // MPPT 1
    // _dc2: DcBlock, // MPPT 2
    // _dc3: DcBlock, // StCha 3
    // _dc4: DcBlock, // StDisCha
    _id_storage_controls: u16,  // = 124
    _storage_block_length: u16, // = 24
    _max_charge_set_watt: u16,
    _max_charge_set_pct: u16,
    _max_discharge_set_pct: u16,
    _storage_control_mode_bitfield: u16, //[u8;2],
    _max_charge_set_va: u16,
    _min_reserve_set_pct: u16,
    charge_state_pct: u16,
    _charge_state_ah: u16,
    _battery_voltage: u16,
    _charge_status_enum: u16,
    _max_discharge_rate_pct: i16,
    _max_charge_rate_pct: i16, // Percent of max charging rate. default is 100
    _charge_rate_time_window: u16, // Time window for charge/discharge rate change. (not supported)
    _charge_rate_timeout: u16, // Timeout period for charge/discharge rate.
    _charge_rate_ramp: u16, // Ramp time for moving from current setpoint to new setpoint.(not supported)
    _charge_from_grid_set_flag: u16, // 0: PV (Charging from grid disabled), 1: GRID (Charging from grid enabled)
    _max_charge_scale: i16,
    _max_charge_discharge_scale: i16,
    _max_charge_va_scale: i16,
    _min_reserve_scale: i16,
    charge_state_pct_scale: i16,
    _charge_state_wh_scale: i16,
    // _id_end_block: u16,
    // _model_block_length: u16,
    // _search: [u8;4],
}

#[derive(BinRead, Debug, Default)]
struct DcBlock {
    id: u16,
    _id_string: [char; 16],
    current: u16,
    voltage: u16,
    power: u16,
    energy: u32,
    _timestamp_secs: u32,
    _temperature_degc: i16, //[u8;2] = [135, 115] ; i16 = -30861 ; u16 = 34675
    operating_state: u16,
    module_events: u32,
}

#[derive(BinRead, Debug)]
struct SmartMeterBlock {
    // _sid: [char;4], // = 'SunS'
    // _id: u16, // = 1
    // _length_common_block: u16, // =65
    // _manifacturer: [char; 32], // = Fronius
    // _model: [char; 32], // = Smart Meter TS 65A-3
    // _firmware_version: [char; 16], // = primary>
    // _software_version: [char; 16], // = 1.3
    // _serial_number: [char; 32],
    // _device_address: u16, // always = 240?
    // _phase_count_plus_210: u16, // = 213
    // _inverter_bock_length: u16, // = 124
    // above = registers 40000 to 40070 -> don't need to read those, static stuff.
    current_total: f32,
    current_phase_a: f32,
    current_phase_b: f32,
    current_phase_c: f32,
    voltage_average_phase_to_neutral: f32,
    voltage_phase_a: f32,
    voltage_phase_b: f32,
    voltage_phase_c: f32,
    voltage_average_phase_to_phase: f32,
    voltage_phase_ab: f32,
    voltage_phase_bc: f32,
    voltage_phase_ca: f32,
    frequency_hz: f32,
    power_watt: f32,
    power_phase_a_watt: f32,
    power_phase_b_watt: f32,
    power_phase_c_watt: f32,
    apparent_power_va: f32,
    apparent_power_phase_a_va: f32,
    apparent_power_phase_b_va: f32,
    apparent_power_phase_c_va: f32,
    reactive_power_var: f32,
    reactive_power_phase_a_var: f32,
    reactive_power_phase_b_var: f32,
    reactive_power_phase_c_var: f32,
    _power_factor_cos: f32,
    _power_factor_phase_a_cos: f32,
    _power_factor_phase_b_cos: f32,
    _power_factor_phase_c_cos: f32,
    energy_exported_total: f32,
    _watthours_exported_phase_a: [u8; 4], //f32, //unsupported, always = 7FC00000 //
    _watthours_exported_phase_b: [u8; 4], //f32, //unsupported, always = 7FC00000 //
    _watthours_exported_phase_c: [u8; 4], //f32, //unsupported, always = 7FC00000 //
    energy_imported_total: f32,
    // _watthours_imported_phase_a: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _watthours_imported_phase_b: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _watthours_imported_phase_c: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _va_hours_exported_total: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _va_hours_exported_phase_a: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _va_hours_exported_phase_b: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _va_hours_exported_phase_c: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _va_hours_imported_total: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _va_hours_imported_phase_a: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _va_hours_imported_phase_b: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _va_hours_imported_phase_c: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_exported_q1_total: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_exported_q1_phase_a: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_exported_q1_phase_b: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_exported_q1_phase_c: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_imported_q1_total: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_imported_q1_phase_a: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_imported_q1_phase_b: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_imported_q1_phase_c: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_exported_q2_total: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_exported_q2_phase_a: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_exported_q2_phase_b: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_exported_q2_phase_c: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_imported_q2_total: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_imported_q2_phase_a: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_imported_q2_phase_b: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_imported_q2_phase_c: [u8;4], //f32, //unsupported, always = 7FC00000 //

    // somewhere below we surpass the 125 registers / 250 bytes limit. but there's nothing useful below anyway:
    // _var_hours_exported_q3_total: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_exported_q3_phase_a: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_exported_q3_phase_b: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_exported_q3_phase_c: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_imported_q3_total: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_imported_q3_phase_a: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_imported_q3_phase_b: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_imported_q3_phase_c: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_exported_q4_total: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_exported_q4_phase_a: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_exported_q4_phase_b: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_exported_q4_phase_c: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_imported_q4_total: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_imported_q4_phase_a: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_imported_q4_phase_b: [u8;4], //f32, //unsupported, always = 7FC00000 //
    // _var_hours_imported_q4_phase_c: [u8;4], //f32, //unsupported, always = 7FC00000 //
}

#[derive(Debug)]
struct FroniusData {
    common_block_nanos: u128,
    common_block: CommonBlock,
    status_block_nanos: u128,
    status_block: StatusBlock,
    mppt_block_nanos: u128,
    mppt_block: MpptBlock,
    smart_meter_block_nanos: u128,
    smart_meter_block: SmartMeterBlock,
}

async fn fronius_read(ctx: &mut Context) -> FroniusData {
    // Address 39999: IllegalDataAddress
    // length >= 126: IllegalDataValue
    ctx.set_slave(Slave::from(1));
    let common_block_nanos = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_nanos();
    let common_block_register = ctx.read_holding_registers(40071, 58).await.unwrap();
    let common_block_bytes: Vec<u8> = common_block_register
        .iter()
        .flat_map(|x| x.iter())
        .flat_map(|b| b.to_be_bytes())
        .collect();
    let common_block: CommonBlock = Cursor::new(common_block_bytes).read_be().unwrap();

    let status_block_nanos = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_nanos();
    let status_block_register = ctx.read_holding_registers(40193, 44).await.unwrap();
    let status_block_bytes: Vec<u8> = status_block_register
        .iter()
        .flat_map(|x| x.iter())
        .flat_map(|b| b.to_be_bytes())
        .collect();
    let status_block: StatusBlock = Cursor::new(status_block_bytes).read_be().unwrap();

    let mppt_block_nanos = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_nanos();
    let mppt_block_register = ctx.read_holding_registers(40265, 112).await.unwrap();
    let mppt_block_bytes: Vec<u8> = mppt_block_register
        .iter()
        .flat_map(|x| x.iter())
        .flat_map(|b| b.to_be_bytes())
        .collect();
    let mppt_block: MpptBlock = Cursor::new(mppt_block_bytes).read_be().unwrap();

    ctx.set_slave(Slave::from(240));
    let smart_meter_block_nanos = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_nanos();
    let smart_meter_block_register = ctx.read_holding_registers(40071, 68).await.unwrap();
    let smart_meter_block_bytes: Vec<u8> = smart_meter_block_register
        .iter()
        .flat_map(|x| x.iter())
        .flat_map(|b| b.to_be_bytes())
        .collect();
    let smart_meter_block: SmartMeterBlock =
        Cursor::new(smart_meter_block_bytes).read_be().unwrap();

    FroniusData {
        common_block_nanos,
        common_block,
        status_block_nanos,
        status_block,
        mppt_block_nanos,
        mppt_block,
        smart_meter_block_nanos,
        smart_meter_block,
    }
}

async fn fronius_send(data: &FroniusData, tx: &Sender<Measurement>) {
    //// can't use the all-at-once loop design because each block has it's own time!
    // for logable in data
    //     .common_block
    //     .logables()
    //     .drain(..)
    //     .chain(data.status_block.logables().drain(..))
    //     .chain(data.mppt_block.logables().drain(..))
    //     .chain(data.smart_meter_block.logables().drain(..))
    // {
    //     tx.send(Measurement::new(
    //         logable.prefix.into(),
    //         logable.value,
    //         data.common_block_nanos,
    //     ))
    //     .await
    //     .ok();
    // }

    for logable in data.common_block.logables() {
        tx.send(Measurement::new(
            logable.prefix.into(),
            logable.value,
            data.common_block_nanos,
            #[cfg(feature = "track_source")]
            crate::MeasurementSource::Fronius,
        ))
        .await
        .ok();
    }
    for logable in data.status_block.logables() {
        tx.send(Measurement::new(
            logable.prefix.into(),
            logable.value,
            data.status_block_nanos,
            #[cfg(feature = "track_source")]
            crate::MeasurementSource::Fronius,
        ))
        .await
        .ok();
    }
    for logable in data.mppt_block.logables() {
        tx.send(Measurement::new(
            logable.prefix.into(),
            logable.value,
            data.mppt_block_nanos,
            #[cfg(feature = "track_source")]
            crate::MeasurementSource::Fronius,
        ))
        .await
        .ok();
    }
    for logable in data.smart_meter_block.logables() {
        tx.send(Measurement::new(
            logable.prefix.into(),
            logable.value,
            data.smart_meter_block_nanos,
            #[cfg(feature = "track_source")]
            crate::MeasurementSource::Fronius,
        ))
        .await
        .ok();
    }
}

struct Logable<'a> {
    //id: Cow<'a, str>,
    prefix: Cow<'a, str>,
    value: f64,
}

impl CommonBlock {
    fn logables(&self) -> Vec<Logable> {
        vec![
            Logable {
                //id: Borrowed("40071"),
                prefix: Borrowed(",device=AC,phase=sum fronius.current"),
                value: self.ac_current_ampere_total as _,
            },
            Logable {
                //id: Borrowed("40073"),
                prefix: Borrowed(",device=AC,phase=A fronius.current"),
                value: self.ac_current_ampere_phase_a as _,
            },
            Logable {
                //id: Borrowed("40075"),
                prefix: Borrowed(",device=AC,phase=B fronius.current"),
                value: self.ac_current_ampere_phase_b as _,
            },
            Logable {
                //id: Borrowed("40077"),
                prefix: Borrowed(",device=AC,phase=C fronius.current"),
                value: self.ac_current_ampere_phase_c as _,
            },
            Logable {
                //id: Borrowed("40079"),
                prefix: Borrowed(",device=AC,phase=AB fronius.voltage"),
                value: self.ac_volt_phase_ab as _,
            },
            Logable {
                //id: Borrowed("40081"),
                prefix: Borrowed(",device=AC,phase=BC fronius.voltage"),
                value: self.ac_volt_phase_bc as _,
            },
            Logable {
                //id: Borrowed("40083"),
                prefix: Borrowed(",device=AC,phase=CA fronius.voltage"),
                value: self.ac_volt_phase_ca as _,
            },
            Logable {
                //id: Borrowed("40085"),
                prefix: Borrowed(",device=AC,phase=A fronius.voltage"),
                value: self.ac_volt_phase_a as _,
            },
            Logable {
                //id: Borrowed("40087"),
                prefix: Borrowed(",device=AC,phase=B fronius.voltage"),
                value: self.ac_volt_phase_b as _,
            },
            Logable {
                //id: Borrowed("40089"),
                prefix: Borrowed(",device=AC,phase=C fronius.voltage"),
                value: self.ac_volt_phase_c as _,
            },
            Logable {
                //id: Borrowed("40093"),
                prefix: Borrowed(",device=AC fronius.frequency"),
                value: self.ac_frequency_hz as _,
            },
            Logable {
                //id: Borrowed("40091"),
                prefix: Borrowed(",device=AC,type=watt fronius.power"),
                value: self.ac_power_watt as _,
            },
            Logable {
                //id: Borrowed("40095"),
                prefix: Borrowed(",device=AC,type=apparent fronius.power"),
                value: self.apparent_power_va as _,
            },
            Logable {
                //id: Borrowed("40097"),
                prefix: Borrowed(",device=AC,type=reactive fronius.power"),
                value: self.reactive_power_var as _,
            },
            Logable {
                //id: Borrowed("40099"),
                prefix: Borrowed(",device=AC fronius.powerFactorPct"),
                value: self.power_factor_pct as _,
            },
            Logable {
                //id: Borrowed("40101"),
                prefix: Borrowed(",device=AC fronius.lifetimeProduction"),
                value: self.ac_lifetime_production as _,
            },
            Logable {
                //id: Borrowed("40107"),
                prefix: Borrowed(",device=DC fronius.voltage"),
                value: self.dc_watt as _,
            },
            Logable {
                //id: Borrowed("40109"),
                prefix: Borrowed(",device=DC fronius.temperature"),
                value: self.temperature_cabinet_degc as _,
            },
            Logable {
                //id: Borrowed("40117"),
                prefix: Borrowed(",type=operation fronius.state"),
                value: self.operation_state as _,
            },
            Logable {
                //id: Borrowed("40118"),
                prefix: Borrowed(",type=operationVendor fronius.state"),
                value: self.operation_state_vendor as _,
            },
            Logable {
                //id: Borrowed("40119"),
                prefix: Borrowed(",type=common fronius.events"),
                value: self.event_flags as _,
            },
            Logable {
                //id: Borrowed("40123"),
                prefix: Borrowed(",type=commonVendor fronius.events"),
                value: self.event_flags_vendor as _,
            },
        ]
    }
}

impl StatusBlock {
    fn logables(&self) -> Vec<Logable> {
        vec![
            Logable {
                //id: Borrowed("40193"),
                prefix: Borrowed(",device=PV_inverter fronius.status"),
                value: self.pv_inverter_status as _,
            },
            Logable {
                //id: Borrowed("40194"),
                prefix: Borrowed(",device=storage_inverter fronius.status"),
                value: self.storage_inverter_status as _,
            },
            Logable {
                //id: Borrowed("40195"),
                prefix: Borrowed(",device=ecp_connection fronius.status"),
                value: self.ecp_connection_status as _,
            },
            Logable {
                //id: Borrowed("40196"),
                prefix: Borrowed(" fronius.lifetimeEnergyOutput"),
                value: self.ac_lifetime_active_real_energy_output_wh as _,
            },
            Logable {
                //id: Borrowed("40224"),
                prefix: Borrowed(" fronius.setpointLimitsReached"),
                value: self.setpoint_limits_reached_bitmask as _,
            },
            Logable {
                //id: Borrowed("40226"),
                prefix: Borrowed(" fronius.activeInverterControls"),
                value: self.active_inverter_controls_bitmask as _,
            },
            Logable {
                //id: Borrowed("40234"),
                prefix: Borrowed(" fronius.voltageRideThroughModesAcive"),
                value: self.voltage_ride_through_modes_active as _,
            },
            Logable {
                //id: Borrowed("40235"),
                prefix: Borrowed(" fronius.isolationResistance"),
                value: self.isolation_resistance as f64
                    * (10_f64.powf(self.isolation_resistance_scale as _)),
            },
        ]
    }
}

impl MpptBlock {
    fn logables(&self) -> Vec<Logable> {
        let mut r = Vec::new();
        r.push(Logable {
            //id: Borrowed("40269"),
            prefix: Borrowed(",type=MpptGlobal fronius.events"),
            value: self.global_events as _,
        });
        let current_scale = 10_f64.powf(self.current_scale as _);
        let voltage_scale = 10_f64.powf(self.voltage_scale as _);
        let power_scale = 10_f64.powf(self.power_scale as _);
        let energy_scale = 10_f64.powf(self.energy_scale as _);
        for dc in &self.dc {
            r.push(Logable {
                //id: Owned(format!("{}", 40282 + (dc.id - 1) * 20)),
                prefix: Owned(format!(",device=DC{} fronius.current", dc.id)),
                value: dc.current as f64 * current_scale,
            });
            r.push(Logable {
                //id: Owned(format!("{}", 40283 + (dc.id - 1) * 20)),
                prefix: Owned(format!(",device=DC{} fronius.voltage", dc.id)),
                value: dc.voltage as f64 * voltage_scale,
            });
            r.push(Logable {
                //id: Owned(format!("{}", 40284 + (dc.id - 1) * 20)),
                prefix: Owned(format!(",device=DC{},type=watt fronius.power", dc.id)),
                value: dc.power as f64 * power_scale,
            });
            r.push(Logable {
                //id: Owned(format!("{}", 40285 + (dc.id - 1) * 20)),
                prefix: Owned(format!(",device=DC{} fronius.energy", dc.id)),
                value: dc.energy as f64 * energy_scale,
            });
            r.push(Logable {
                //id: Owned(format!("{}", 40290 + (dc.id - 1) * 20)),
                prefix: Owned(format!(
                    ",device=DC{},type=MpptOperating fronius.state",
                    dc.id
                )),
                value: dc.operating_state as _,
            });
            r.push(Logable {
                //id: Owned(format!("{}", 40291 + (dc.id - 1) * 20)),
                prefix: Owned(format!(",device=DC{},type=module fronius.events", dc.id)),
                value: dc.module_events as _,
            });
        }
        r.push(Logable {
            //id: Borrowed("40361"),
            prefix: Borrowed(" fronius.chargeState"),
            value: self.charge_state_pct as f64 * 10_f64.powf(self.charge_state_pct_scale as _),
        });
        r
    }
}

impl SmartMeterBlock {
    fn logables(&self) -> Vec<Logable> {
        vec![
            Logable {
                //id: Borrowed("sm40071"),
                prefix: Borrowed(",device=smartMeter,phase=sum fronius.current"),
                value: self.current_total as _,
            },
            Logable {
                //id: Borrowed("sm40073"),
                prefix: Borrowed(",device=smartMeter,phase=A fronius.current"),
                value: self.current_phase_a as _,
            },
            Logable {
                //id: Borrowed("sm40075"),
                prefix: Borrowed(",device=smartMeter,phase=B fronius.current"),
                value: self.current_phase_b as _,
            },
            Logable {
                //id: Borrowed("sm40077"),
                prefix: Borrowed(",device=smartMeter,phase=C fronius.current"),
                value: self.current_phase_c as _,
            },
            Logable {
                //id: Borrowed("sm40079"),
                prefix: Borrowed(",device=smartMeter,phase=average_to-neutral fronius.voltage"),
                value: self.voltage_average_phase_to_neutral as _,
            },
            Logable {
                //id: Borrowed("sm40081"),
                prefix: Borrowed(",device=smartMeter,phase=A fronius.voltage"),
                value: self.voltage_phase_a as _,
            },
            Logable {
                //id: Borrowed("sm40083"),
                prefix: Borrowed(",device=smartMeter,phase=B fronius.voltage"),
                value: self.voltage_phase_b as _,
            },
            Logable {
                //id: Borrowed("sm40085"),
                prefix: Borrowed(",device=smartMeter,phase=C fronius.voltage"),
                value: self.voltage_phase_c as _,
            },
            Logable {
                //id: Borrowed("sm40087"),
                prefix: Borrowed(",device=smartMeter,phase=average_interphase fronius.voltage"),
                value: self.voltage_average_phase_to_phase as _,
            },
            Logable {
                //id: Borrowed("sm40089"),
                prefix: Borrowed(",device=smartMeter,phase=AB fronius.voltage"),
                value: self.voltage_phase_ab as _,
            },
            Logable {
                //id: Borrowed("sm40091"),
                prefix: Borrowed(",device=smartMeter,phase=BC fronius.voltage"),
                value: self.voltage_phase_bc as _,
            },
            Logable {
                //id: Borrowed("sm40093"),
                prefix: Borrowed(",device=smartMeter,phase=CA fronius.voltage"),
                value: self.voltage_phase_ca as _,
            },
            Logable {
                //id: Borrowed("sm40095"),
                prefix: Borrowed(",device=smartMeter fronius.frequency"),
                value: self.frequency_hz as _,
            },
            Logable {
                //id: Borrowed("sm40097"),
                prefix: Borrowed(",device=smartMeter,type=watt,phase=sum fronius.power"),
                value: self.power_watt as _,
            },
            Logable {
                //id: Borrowed("sm40099"),
                prefix: Borrowed(",device=smartMeter,type=watt,phase=A fronius.power"),
                value: self.power_phase_a_watt as _,
            },
            Logable {
                //id: Borrowed("sm40101"),
                prefix: Borrowed(",device=smartMeter,type=watt,phase=B fronius.power"),
                value: self.power_phase_b_watt as _,
            },
            Logable {
                //id: Borrowed("sm40103"),
                prefix: Borrowed(",device=smartMeter,type=watt,phase=C fronius.power"),
                value: self.power_phase_c_watt as _,
            },
            Logable {
                //id: Borrowed("sm40105"),
                prefix: Borrowed(",device=smartMeter,type=apparent,phase=sum fronius.power"),
                value: self.apparent_power_va as _,
            },
            Logable {
                //id: Borrowed("sm40107"),
                prefix: Borrowed(",device=smartMeter,type=apparent,phase=A fronius.power"),
                value: self.apparent_power_phase_a_va as _,
            },
            Logable {
                //id: Borrowed("sm40109"),
                prefix: Borrowed(",device=smartMeter,type=apparent,phase=B fronius.power"),
                value: self.apparent_power_phase_b_va as _,
            },
            Logable {
                //id: Borrowed("sm40111"),
                prefix: Borrowed(",device=smartMeter,type=apparent,phase=C fronius.power"),
                value: self.apparent_power_phase_c_va as _,
            },
            Logable {
                //id: Borrowed("sm40113"),
                prefix: Borrowed(",device=smartMeter,type=reactive,phase=sum fronius.power"),
                value: self.reactive_power_var as _,
            },
            Logable {
                //id: Borrowed("sm40115"),
                prefix: Borrowed(",device=smartMeter,type=reactive,phase=A fronius.power"),
                value: self.reactive_power_phase_a_var as _,
            },
            Logable {
                //id: Borrowed("sm40117"),
                prefix: Borrowed(",device=smartMeter,type=reactive,phase=B fronius.power"),
                value: self.reactive_power_phase_b_var as _,
            },
            Logable {
                //id: Borrowed("sm40119"),
                prefix: Borrowed(",device=smartMeter,type=reactive,phase=C fronius.power"),
                value: self.reactive_power_phase_c_var as _,
            },
            Logable {
                //id: Borrowed("sm40121"),
                prefix: Borrowed(",device=smartMeter,phase=sum fronius.powerFactor"),
                value: self._power_factor_cos as _,
            },
            Logable {
                //id: Borrowed("sm40123"),
                prefix: Borrowed(",device=smartMeter,phase=A fronius.powerFactor"),
                value: self._power_factor_phase_a_cos as _,
            },
            Logable {
                //id: Borrowed("sm40125"),
                prefix: Borrowed(",device=smartMeter,phase=B fronius.powerFactor"),
                value: self._power_factor_phase_b_cos as _,
            },
            Logable {
                //id: Borrowed("sm40127"),
                prefix: Borrowed(",device=smartMeter,phase=C fronius.powerFactor"),
                value: self._power_factor_phase_c_cos as _,
            },
            Logable {
                //id: Borrowed("sm40129"),
                prefix: Borrowed(",device=smartMeter,direction=export fronius.energy"),
                value: self.energy_exported_total as _,
            },
            Logable {
                //id: Borrowed("sm40137"),
                prefix: Borrowed(",device=smartMeter,direction=import fronius.energy"),
                value: self.energy_imported_total as _,
            },
        ]
    }
}

lazy_static! {
    static ref MILLENNIUM: DateTime<FixedOffset> =
        DateTime::parse_from_rfc3339("2000-01-01T00:00:00-00:00").unwrap();
}

pub async fn watcher(addr: SocketAddr, tx: Sender<Measurement>, query_frequency: Duration) {
    let mut ctx = tokio_modbus::prelude::tcp::connect(addr).await.unwrap();
    loop {
        let data = fronius_read(&mut ctx).await;
        fronius_send(&data, &tx).await;
        #[cfg(feature = "fronius_debugging")]
        println!(
            "{:#?}\n{}",
            data,
            (*MILLENNIUM
                + TimeDelta::try_seconds(data.status_block._seconds_since_y2k as i64).unwrap())
            .format("%Y-%m-%d %H:%M:%S")
        );

        // Commented out because we didn't see any values different from 0 between 2024-05-17 18:52:15 and 2024-05-18 23:27:15
        // let current_time_nanos = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_nanos();
        // let smart_meter_events = ctx.read_holding_registers(40193, 2).await.unwrap();
        // let smart_meter_events_u32 = ((smart_meter_events[0] as u32) << 16) | (smart_meter_events[1] as u32);
        // tx
        //     .send(format!(",type=smartMeter fronius.events={} {}\n", smart_meter_events_u32, current_time_nanos))
        //     .await
        //     .ok();

        sleep(query_frequency).await;
    }
}
