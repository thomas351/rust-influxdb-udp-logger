// combine multiple if let constructs with each other and/or boolean conditions
#![feature(let_chains)]
// enable hiding normal statements behind feature flags - see issue #15701 <https://github.com/rust-lang/rust/issues/15701> for more information
#![feature(stmt_expr_attributes)]
#![feature(async_closure)]
#![feature(duration_constructors)]

#[cfg(all(
    feature = "alias",
    any(feature = "signal_notifier", feature = "http_server")
))]
use cfg_if::cfg_if;
use chrono::TimeDelta;
#[cfg(any(
    feature = "signal_notifier",
    feature = "http_server",
    feature = "influxdb_relay"
))]
use chrono::{DateTime, Local};
use clap::{Parser, ValueEnum};
use core::fmt;
#[cfg(any(
    feature = "thermal_zones",
    feature = "external",
    feature = "battery",
    feature = "valetudo",
    feature = "sysinfo",
))]
use std::{
    borrow::Cow::{self, Borrowed, Owned},
    fs,
};
use std::{
    io,
    net::{SocketAddr, ToSocketAddrs},
    num::ParseIntError,
    str::{self, FromStr},
    time::Duration,
};
use tokio::{net::TcpStream, sync::mpsc, time::sleep};
#[cfg(feature = "battery")]
mod batteries;
mod data_sender;
#[cfg(feature = "external")]
mod external;
#[cfg(feature = "fronius")]
mod fronius;
#[cfg(feature = "http_server")]
mod http_server;
#[cfg(any(feature = "http_server", feature = "signal_notifier"))]
mod human_readable_duration;
#[cfg(feature = "influxdb_relay")]
mod influxdb_relay;
#[cfg(any(feature = "external", feature = "thermal_zones"))]
mod metrics_sender;
#[cfg(feature = "mqtt_relay")]
mod mqtt_relay;
#[cfg(feature = "signal_notifier")]
mod signal_notifier;
#[cfg(feature = "sysinfo")]
mod sysinfo;
#[cfg(feature = "thermal_zones")]
mod thermal_zones;
#[cfg(feature = "valetudo")]
mod valetudo;
#[cfg(feature = "worx")]
mod worx;

// test specific feature without changing Cargo.toml default line:
// cargo run --no-default-features -F battery,http_server,mqtt_relay
// cargo run --no-default-features -F battery,sysinfo,thermal_zones,http_server
// cargo run --release --no-default-features -F battery,sysinfo,thermal_zones,http_server -- -t 192.168.16.4:8089 -H 8080
// cargo run --release --no-default-features -F battery,sysinfo,thermal_zones,http_server -- -H 8080
// cargo run --release --no-default-features -F battery,sysinfo,thermal_zones,http_server,alias -- -H 8080 -a Lenovo_P16v.temperature=temp
// cargo run --release --no-default-features -F battery,sysinfo,thermal_zones,http_server,alias,alias_regex,alias_regex_debugging -- -H 8080 -a ",zone=(.*) Lenovo_P16v.temperature=temp_\1"
// cargo run --release --no-default-features -F battery,sysinfo,thermal_zones,http_server,subscription_sender -- --subscription-port=1234

// Roborock Q7 = armv7l hf Ubuntu 14.04.3LTS with glibc v2.17:
// can't use --target armv7-unknown-linux-gnueabihf because targeted gnu libc versions don't match: 2.25, 2.28 & 2.29
// # cross build --target armv7-unknown-linux-musleabihf --release
// # scp -O target/armv7-unknown-linux-musleabihf/release/rust-influxdb-udp-logger root@rrq7:/mnt/data/
// # /mnt/data/rust-influxdb-udp-logger --hostname roborock_q7 --skip-battery --valetudo localhost:80 --protocol tcp --class-zone-devisor 1 --sysinfo-zone-devisor 0.001 --target 192.168.16.4:8089 >> /tmp/victoria_logger.log 2>&1 &

// Dreame = aarch64, Athena Linux (p2029_release)
// # cross build --target aarch64-unknown-linux-musl --release #can't use gnu because of libc mismatch. gnu:centos works, but results in larger binary than musl
// # scp -O target/aarch64-unknown-linux-musl/release/rust-influxdb-udp-logger dreame:/data/
// # /data/rust-influxdb-udp-logger --hostname dreame --valetudo localhost:80 --protocol tcp --target 192.168.16.4:8089 >> /tmp/victoria_logger.log 2>&1 &

// pi4, Debian 11 bullseye $(cat /etc/os-release) GLIBC 2.31 $(ldd --version)
// # RUSTFLAGS="-Zlocation-detail=none" cross build --target aarch64-unknown-linux-gnu --release --no-default-features -F battery,sysinfo,thermal_zones,http_server,fronius,external
// RUST_BACKTRACE=1 ./rust-influxdb-udp-logger --hostname=pi4 --udp-target=localhost:8089 --external="gpu;1;temp=(.*)'C;vcgencmd;measure_temp" --external="pmic;1;temp=(.*)'C;vcgencmd;measure_temp;pmic" --external="osc;1;.*\((.*)'C\);vcgencmd;read_ring_osc"
// read_ring_osc(2)=2.264MHz (@0.8500V) (39.9'C)

// silence default stuff to test some distinct features:
// # cargo run -- --skip-sysinfo --skip-class-zones --skip-battery

// parameters for linux pc with nvidia gpu:
// # cargo run -- --hostname=nvidiapc --udp-target=influxdb-host:8089 --skip-zone=0 --skip-zone=2 --external="nvidia;1;(.*);nvidia-smi;--query-gpu=temperature.gpu;--format=csv,noheader"

// TODO, feature wishlist:
// add feature to show recent history instead of only last value for each metric on http-server status page, maybe with a table column with small svg diagrams
// research why closed / broken connections don't bubble up as such anymore
//   last recieved broken connection notification: Aug 4, 2024, 3:22 PM: "Relay TCP Connection closed/broken: 192.168.16.170:51712"
//   when disconnecting killing or KILLing telnet running on laptop connected to pi4 it always produces closed/broken connection notifications.
//   robot has no telnet / netcat to compare, maybe try and find one?
// worx: add mqtt client to subscribe to events in addition to query http endpoint regularly
// signal-notify: switch the cli to it's own phone number instead of a connected account

#[derive(Parser, Debug)]
#[command(author, version, verbatim_doc_comment)]
/// This program produces influxdb lines and sends them via UDP. The concrete format is fitted better for VictoriaMetrics, because we don't use the InfluxDB metrics name, only the field name (which is used as metrics name in VictoriaMetrics because it doesn't have mutliple fields inside a metric.)
///
/// Data sources:
/// * "/sys/class/thermal/thermal_zone*/temp" -> skip some using --skip-class-zone parameter or all using --skip_class_zones
///     on most systems the values are in m°C, use --zone_devisor on other systems.
/// * component temperatures found using the sysinfo library -> skip some using --skip-sysinfo-zone or all using --skip_sysinfo_zones
/// * average 1, 5 & 15 minutes sysload (aquired by sysinfo library)
/// * disk space available (aquired by sysinfo library)
/// * battery stats from /sys/class/power_supply/* (aquired by starship-battery library, thread-safe fork)
/// * external commands specified using --external parameters
struct Cli {
    #[arg(short = 't', long)]
    /// host:port, if not set, we just print to stdout instead.
    target: Option<HostnameSocketAddr>,

    #[arg(short = 'P', long, value_enum, default_value_t = Protocol::Udp)]
    protocol: Protocol,

    #[cfg(feature = "http_sender")]
    #[arg(short = 'u', long)]
    http_user: Option<String>,

    #[cfg(feature = "http_sender")]
    #[arg(short = 'p', long)]
    http_password: Option<String>,

    #[cfg(feature = "http_sender")]
    #[arg(short = 'U', long)]
    http_url: Option<String>,

    #[arg(short = 'n', long)]
    hostname: Option<String>,

    #[cfg(feature = "sysinfo")]
    #[arg(short = 's', long, num_args(0..))]
    skip_sysinfo_zone: Vec<String>,

    #[cfg(all(
        feature = "alias",
        any(feature = "signal_notifier", feature = "http_server")
    ))]
    #[arg(short = 'a', long, num_args(0..))]
    // define aliases that are used for http status page and signal notifications. Enties must use the format: metric=alias
    prefix_alias: Vec<PrefixAlias>,

    #[cfg(feature = "sysinfo")]
    #[arg(short = 'S', long)]
    skip_sysinfo_zones: bool,

    #[cfg(feature = "sysinfo")]
    #[arg(short = 'y', long)]
    skip_sysinfo: bool,

    #[cfg(feature = "battery")]
    #[arg(short = 'b', long)]
    skip_battery: bool,

    #[cfg(feature = "battery")]
    #[arg(short = 'B', long, default_value_t = 0.4)]
    battery_frequency_secs: f32,

    #[cfg(feature = "http_server")]
    #[arg(short = 'H', long)]
    http_status_port: Option<u16>,

    #[cfg(feature = "influxdb_relay")]
    #[arg(short = 'r', long)]
    relay_port: Option<u16>,

    #[arg(long)]
    /// also print the data lines to stdout besides sending them via UDP (has no effect if udp_target=stdout) + setup logging
    stdout_too: bool,

    #[arg(long)]
    /// only send measured values without timestamps, ignoring the local clock (or received timestamps from sources that include them)
    ignore_clock: bool,

    #[cfg(feature = "valetudo")]
    #[arg(short = 'v', long)]
    /// specify target to activate valetudo swagger UI SSE event watchers + WifiConfigurationCompatibility watcher for signal strength
    valetudo: Option<HostnameSocketAddr>,

    #[cfg(feature = "valetudo")]
    #[arg(long, default_value_t = 1.0)]
    valetudo_frequency_secs: f32,

    #[cfg(feature = "fronius")]
    #[arg(short = 'f', long)]
    /// specify target to activate fronius modbus data fetcher
    fronius: Option<HostnameSocketAddr>,

    #[cfg(feature = "fronius")]
    #[arg(short = 'F', long, default_value_t = 1.0)]
    fronius_frequency_secs: f32,

    #[cfg(feature = "external")]
    #[arg(short = 'e', long, num_args(0..))]
    /// format: zone;devisor?;regex_unpacker?;query_frequency?;invalid_values?;cmd;arg1?;..;argN? - ?=optional, use ";;" to skip in-between parts.\n
    /// newly added format for non temperature variables: OTHER;prefix;devisor?;regex_unpacker?;query_frequency?;invalid_values?;cmd;arg1?;..;argN?; - where prefix might contain {} which is replaced by hostname.
    /// multiple invalid_values are split by '+', so for example: -1+0
    external: Vec<external::External>,

    #[cfg(feature = "thermal_zones")]
    #[arg(short = 'c', long, num_args(0..))]
    skip_class_zone: Vec<u8>,

    #[cfg(feature = "thermal_zones")]
    #[arg(short = 'C', long)]
    skip_class_zones: bool,

    #[cfg(feature = "thermal_zones")]
    #[arg(short = 'D', long, default_value_t = 1000.0)]
    /// by default 1000, because on most systems /sys/class/thermal/thermal_zone*/temp" contain values in 1000*x °C
    class_zone_devisor: f64,

    #[cfg(feature = "thermal_zones")]
    #[arg(short = '1', long)]
    /// specify this to omit the zone tag. If you have more then one zone, the data will be mixed / scrambled into a single metric!
    single: bool,

    #[cfg(feature = "thermal_zones")]
    #[arg(short = 'T', long, default_value_t = 0.4)]
    thermal_zones_frequency_secs: f32,

    #[cfg(feature = "sysinfo")]
    #[arg(short = 'Y', long, default_value_t = 1.0)]
    /// by default 1, because on most systems sysinfo gives values in °C (as documented).
    /// Added because on RoboRock Q7 it gives a "sunxi-therm_temp1" value in x/1000 °C
    sysinfo_zone_devisor: f32,

    #[cfg(feature = "sysinfo")]
    #[arg(short = 'l', long, value_enum, default_value_t = Load::OneMinute)]
    /// by default we only log the 1m load, since 5m & 15m is redundant (=can be inferred from 1m)
    load: Load,

    #[cfg(feature = "subscription_sender")]
    #[arg(long)]
    subscription_port: Option<u16>,

    #[cfg(feature = "signal_notifier")]
    #[arg(long)]
    signal_group: Option<String>,

    #[cfg(feature = "signal_notifier")]
    #[arg(short='w',long, num_args(0..))]
    signal_watch: Vec<String>,

    #[cfg(feature = "signal_notifier")]
    #[arg(long, default_value_t = 90)]
    skip_new_metrics_notification_sec: u64,

    #[cfg(feature = "signal_notifier")]
    #[arg(long, default_value_t = 90)]
    min_staleness_sec: u64,

    #[cfg(feature = "signal_notifier")]
    #[arg(long, default_value_t = 2.2)]
    staleness_factor: f64,

    #[cfg(feature = "worx")]
    #[arg(long)]
    worx_user: Option<String>,

    #[cfg(feature = "worx")]
    #[arg(long)]
    worx_password: Option<String>,

    #[cfg(feature = "worx")]
    #[arg(long, default_value_t = 602)]
    /// how often can we expect a new worx status to be available? in seconds.
    worx_frequency: i64,

    #[arg(short = 'm', long, default_value_t = 80.0)]
    /// how often should variables be sent when their value doesn't change? in seconds.
    minimum_frequency: f32,
}

#[cfg(feature = "track_source")]
#[derive(Debug, Clone)]
enum MeasurementSource {
    #[cfg(feature = "valetudo")]
    Valetudo,
    #[cfg(feature = "sysinfo")]
    Sysinfo,
    #[cfg(feature = "thermal_zones")]
    ThermalZones,
    #[cfg(feature = "fronius")]
    Fronius,
    #[cfg(feature = "influxdb_relay")]
    UdpRelay(SocketAddr),
    #[cfg(feature = "influxdb_relay")]
    TcpRelay(SocketAddr),
    #[cfg(feature = "mqtt_relay")]
    MqttRelay,
    #[cfg(feature = "battery")]
    Battery,
    #[cfg(feature = "external")]
    ExternalCommand,
    #[cfg(feature = "worx")]
    Worx,
}

#[cfg(feature = "track_source")]
impl fmt::Display for MeasurementSource {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            #[cfg(feature = "influxdb_relay")]
            Self::UdpRelay(sock) => write!(f, "UDP: {}", sock),
            #[cfg(feature = "influxdb_relay")]
            Self::TcpRelay(sock) => write!(f, "TCP: {}", sock),
            s => write!(f, "{:?}", s),
        }
    }
}

#[derive(Debug)]
struct Measurement {
    prefix: String,
    value: f64,
    time: u128,
    #[cfg(feature = "auto_resend")]
    auto_resend: bool,
    #[cfg(feature = "track_source")]
    src: MeasurementSource,
}

impl Measurement {
    fn new(
        prefix: String,
        value: f64,
        time: u128,
        #[cfg(feature = "track_source")] src: MeasurementSource,
    ) -> Self {
        Self {
            prefix,
            value,
            time,
            #[cfg(feature = "auto_resend")]
            auto_resend: false,
            #[cfg(feature = "track_source")]
            src,
        }
    }
    #[cfg(feature = "valetudo")]
    fn auto_resend(
        prefix: String,
        value: f64,
        time: u128,
        #[cfg(feature = "track_source")] src: MeasurementSource,
    ) -> Self {
        Self {
            prefix,
            value,
            time,
            #[cfg(feature = "auto_resend")]
            auto_resend: true,
            #[cfg(feature = "track_source")]
            src,
        }
    }
    fn format(&self, no_time: bool) -> String {
        if no_time {
            format!("{}={}\n", self.prefix, self.value)
        } else {
            format!("{}={} {}\n", self.prefix, self.value, self.time)
        }
    }
}

impl fmt::Display for Measurement {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "{}={} {}", self.prefix, self.value, self.time)
    }
}

#[cfg(feature = "sysinfo")]
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum, Debug)]
enum Load {
    None,
    All,
    OneMinute,
    FiveMinutes,
    FifteenMinutes,
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum, Debug)]
enum Protocol {
    Udp,
    Tcp,
    #[cfg(feature = "http_sender")]
    Http,
}

#[derive(Debug, Clone)]
struct HostnameSocketAddr {
    addr: SocketAddr,
}

impl FromStr for HostnameSocketAddr {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (host, port_str) = match s.find(':') {
            Some(i) => (&s[..i], &s[i + 1..]),
            None => return Err("socket address expected in this format: 'host:port'".to_owned()),
        };
        let port: u16 = port_str
            .parse()
            .map_err(|e: ParseIntError| format!("couldn't parse port {port_str}: {e}"))?;
        let mut addr = (host, port).to_socket_addrs().map_err(|e| e.to_string())?;
        let addr = addr
            .next()
            .ok_or_else(|| format!("could not resolve hostname = {host}"))?;
        Ok(HostnameSocketAddr { addr })
    }
}

#[cfg(all(
    feature = "alias",
    any(feature = "signal_notifier", feature = "http_server")
))]
#[derive(Debug, Clone)]
struct PrefixAlias {
    metric: String,
    alias: String,
    #[cfg(feature = "alias_regex")]
    regex: bool,
}
#[cfg(all(
    feature = "alias",
    any(feature = "signal_notifier", feature = "http_server")
))]
impl FromStr for PrefixAlias {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        #[cfg(feature = "alias_regex_debugging")]
        log(&format!("parsing alias: {s}"));
        #[cfg(feature = "alias_regex")]
        if let Some((metric, alias)) = s.rsplit_once(';') {
            return Ok(Self {
                metric: metric.into(),
                alias: alias.into(),
                regex: true,
            });
        };
        #[cfg(feature = "alias_regex_debugging")]
        log(&format!("did not contain semicolon? {s}"));
        if let Some((metric, alias)) = s.rsplit_once('=') {
            Ok(Self {
                metric: metric.into(),
                alias: alias.into(),
                #[cfg(feature = "alias_regex")]
                regex: false,
            })
        } else {
            cfg_if! {
                if #[cfg(feature = "alias_regex")] {
                    Err("MetricAlias needs to be either defined with regex format: metric;alias or with plain text format metric=alias. metric is allowed to contain additional = signs, alias must not. Plaintext version must not contain ';'. (split at last = occurrence)".into())
                }
                else {
                    Err("MetricAlias needs to be defined with format: metric=alias. metric is allowed to contain additional = signs, alias must not. (split at last = occurrence)".into())
                }
            }
        }
    }
}

#[cfg(all(feature = "alias", feature = "signal_notifier"))]
fn apply_aliases(prefix: &str, prefix_aliases: &Vec<PrefixAlias>) -> String {
    apply_aliases_opt(prefix, prefix_aliases).unwrap_or(prefix.into())
}

#[cfg(all(
    feature = "alias",
    any(feature = "signal_notifier", feature = "http_server")
))]
fn apply_aliases_opt(prefix: &str, prefix_aliases: &Vec<PrefixAlias>) -> Option<String> {
    #[cfg(feature = "alias_regex_debugging")]
    log(&format!("apply_aliases: {prefix} - {prefix_aliases:?}"));
    let mut changed: Option<String> = None;
    for alias in prefix_aliases {
        #[cfg(feature = "alias_regex")]
        if alias.regex {
            match regex::Regex::new(&alias.metric) {
                Ok(regex) => {
                    if let Some(captures) = regex.captures(prefix) {
                        if changed.is_none() {
                            changed = Some(prefix.into());
                        }
                        let complete_match = captures.get(0).unwrap().as_str();
                        let mut replacement = alias.alias.clone();
                        for (idx, current_match) in captures.iter().enumerate() {
                            let current_match = match current_match {
                                Some(v) => v.as_str(),
                                None => "",
                            };
                            replacement = replacement.replace(&format!("\\{idx}"), current_match);
                            // let changed_str = changed.as_ref().unwrap();
                            // let applied_replacement = alias.alias.replace(&format!("\\{idx}"), current_match);
                            // let new_changed = changed_str.replace(complete_match, &applied_replacement);
                            // let c3 = c1.replace(from, to)
                            // #[cfg(feature = "alias_regex_debugging")]
                            // log(&format!("replaced \\{idx} in {c1} with {current_match} --> {applied_replacement}"));
                            // changed.replace(changed.replace(complete_match,applied_replacement));
                        }
                        changed = Some(
                            changed
                                .as_ref()
                                .unwrap()
                                .replace(complete_match, &replacement),
                        );
                    }
                    // regex.replace_all(haystack, rep) // TODO: FIXME: use fancy_regex for backreference replacement?
                }
                Err(e) => log_error(&format!("invalid regex '{}', error: {e}", alias.metric)),
            };
            continue;
        }
        if prefix.contains(&alias.metric) {
            changed = Some(changed.map_or_else(
                || prefix.replace(&alias.metric, &alias.alias),
                |c: String| c.replace(&alias.metric, &alias.alias),
            ));
        }
    }
    changed
}

impl fmt::Display for HostnameSocketAddr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.addr)
    }
}

fn format_now() -> String {
    chrono::Local::now()
        .format("%Y-%m-%d %H:%M:%S%.3f")
        .to_string()
}

#[cfg(any(
    feature = "signal_notifier",
    feature = "http_server",
    feature = "influxdb_relay"
))]
fn format_nanos(nanos: i64) -> String {
    let ts: DateTime<Local> = DateTime::from_timestamp_nanos(nanos).into();
    ts.format("%Y-%m-%d %H:%M:%S%.3f").to_string()
}

#[cfg(any(
    feature = "main_debugging",
    feature = "sender_debugging",
    feature = "thermal_zones_debugging",
    feature = "battery_debugging",
    feature = "valetudo_debugging",
    feature = "alias_regex_debugging",
    feature = "valetudo",
    feature = "influxdb_relay",
    feature = "mqtt_relay",
    feature = "signal_notifier",
    feature = "subscription_sender",
    feature = "worx",
))]
fn log(msg: &str) {
    println!("{} - {}", format_now(), msg);
}

fn log_error(msg: &str) {
    eprintln!("{} - {}", format_now(), msg);
}

#[tokio::main]
async fn main() -> io::Result<()> {
    let mut handles = vec![];

    #[cfg(any(
        feature = "external",
        feature = "thermal_zones",
        feature = "sysinfo",
        feature = "valetudo",
        feature = "fronius",
        feature = "worx",
    ))]
    let mut args = Cli::parse();
    #[cfg(not(any(
        feature = "external",
        feature = "thermal_zones",
        feature = "sysinfo",
        feature = "valetudo",
        feature = "fronius",
        feature = "worx",
    )))]
    let args = Cli::parse();

    // https://www.koderhq.com/tutorial/rust/concurrency/
    // create send/receiver vars to move data through channel
    // synchronous channel?:
    let (tx, rx) = mpsc::channel::<Measurement>(1024);
    // async channel?:
    // let (tx, rx) = tokio::sync::oneshot::channel::<String>();

    #[cfg(any(
        feature = "thermal_zones",
        feature = "external",
        feature = "battery",
        feature = "valetudo",
        feature = "sysinfo",
    ))]
    let hostname: Cow<str> = match args.hostname.as_ref() {
        Some(a) => Owned(a.clone()),
        None => match fs::read_to_string("/etc/hostname") {
            Ok(b) => Owned(b.trim().to_owned()),
            _ => Borrowed("unknown"),
        },
    };

    #[cfg(feature = "main_debugging")]
    log("args processed, starting setup");

    #[cfg(feature = "signal_notifier")]
    let (signal_tx, signal_rx) = mpsc::channel::<String>(128);

    #[cfg(feature = "influxdb_relay")]
    if let Some(port) = args.relay_port {
        handles.append(
            &mut influxdb_relay::setup(
                port,
                &tx,
                #[cfg(feature = "signal_notifier")]
                signal_tx.clone(),
            )
            .await,
        );
    }

    #[cfg(feature = "thermal_zones")]
    if !args.skip_class_zones {
        handles.append(&mut thermal_zones::setup(
            &tx,
            args.skip_class_zone.drain(..).collect(),
            &hostname,
            args.class_zone_devisor,
            args.single,
            Duration::from_secs_f32(args.thermal_zones_frequency_secs),
        ));
    }

    #[cfg(feature = "main_debugging")]
    log("file watchers setup, next external");

    #[cfg(feature = "external")]
    handles.append(&mut external::setup(
        args.external.drain(..).collect(),
        &hostname,
        &tx,
    ));

    #[cfg(feature = "main_debugging")]
    log("externals setup, next sys");

    #[cfg(feature = "battery")]
    if !args.skip_battery {
        handles.push(tokio::spawn(batteries::watcher(
            hostname.as_ref().to_owned(),
            Duration::from_secs_f32(args.battery_frequency_secs),
            tx.clone(),
        )));
    }

    #[cfg(feature = "mqtt_relay")]
    handles.append(&mut mqtt_relay::setup(tx.clone()));

    #[cfg(feature = "valetudo")]
    if let Some(valetudo_host) = args.valetudo.take() {
        handles.push(tokio::spawn(valetudo::wifi_watcher(
            valetudo_host.addr,
            hostname.as_ref().to_owned(),
            Duration::from_secs_f32(args.valetudo_frequency_secs),
            tx.clone(),
        )));
        handles.push(tokio::spawn(valetudo::state_watcher(
            valetudo_host.addr,
            hostname.as_ref().to_owned(),
            tx.clone(),
            Duration::from_secs_f32(args.minimum_frequency),
        )));
    }

    #[cfg(feature = "fronius")]
    if let Some(fronius_host) = args.fronius.take() {
        handles.push(tokio::spawn(fronius::watcher(
            fronius_host.addr,
            tx.clone(),
            Duration::from_secs_f32(args.fronius_frequency_secs),
        )));
    }

    #[cfg(feature = "sysinfo")]
    if !args.skip_sysinfo {
        handles.push(tokio::spawn(sysinfo::watcher(
            hostname.into_owned(),
            tx.clone(),
            args.skip_sysinfo_zone.drain(..).collect(),
            args.skip_sysinfo_zones,
            args.sysinfo_zone_devisor,
            args.load,
        )));
    }

    #[cfg(feature = "worx")]
    if let Some(usr) = args.worx_user.take()
        && let Some(pwd) = args.worx_password.take()
        && let Some(frequency) = TimeDelta::new(args.worx_frequency, 0)
    {
        handles.push(tokio::spawn(worx::http_client_loop(
            usr, pwd, tx, frequency,
        )));
    }

    #[cfg(feature = "main_debugging")]
    log("watches setup complete, now we run the data_sender loop");

    data_sender::sender_loop(
        rx,
        args,
        #[cfg(feature = "signal_notifier")]
        signal_tx,
        #[cfg(feature = "signal_notifier")]
        signal_rx,
    )
    .await;
    futures::future::join_all(handles).await;

    log_error("rust main end reached! this should not happen!");
    Ok(())
}

async fn tcp_connect(addr: &SocketAddr) -> TcpStream {
    loop {
        match TcpStream::connect(addr).await {
            Ok(stream) => return stream,
            Err(err) => {
                println!("{} - Failed to connect: {}", format_now(), err);
                sleep(Duration::from_secs(1)).await;
            }
        }
    }
}
