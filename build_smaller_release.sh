
#cargo clean

#target=x86_64-unknown-linux-gnu
#target=x86_64-pc-windows-gnu
#features=thermal_zones
#features=sysinfo

## Roborock Q7 = armv7l hf Ubuntu 14.04.3LTS with glibc v2.17:
#target=armv7-unknown-linux-musleabihf
#features=valetudo,valetudo_filter_unchanged,sysinfo,thermal_zones,http_server,track_source
# scp -O target/armv7-unknown-linux-musleabihf/release/rust-influxdb-udp-logger root@rrq7:/mnt/data/
# /mnt/data/rust-influxdb-udp-logger --hostname roborock_q7 --ignore-clock --valetudo localhost:80 --protocol tcp --class-zone-devisor 1 --sysinfo-zone-devisor 0.001 --target 192.168.16.4:8089 >> /tmp/victoria_logger.log 2>&1 &
# 2024-08-13: 648460 -> 283328 = 648kb -> 283kb

## Dreame = aarch64, Athena Linux (p2029_release)
#target=aarch64-unknown-linux-musl #can't use gnu because of libc mismatch. gnu:centos works, but results in larger binary than musl
#target=aarch64-unknown-linux-gnu # 2024-08-16: gnu works, musl doesn't compile: stdio/vfprintf.c:*: undefined reference to __addtf3, __netf2, __subtf3, __fixtfsi, __floatsitf, __multf3, ..
#features=battery,valetudo,valetudo_filter_unchanged,sysinfo,thermal_zones,http_server,track_source,subscription_sender
#features=valetudo,valetudo_filter_unchanged,sysinfo,thermal_zones
# scp -O target/aarch64-unknown-linux-musl/release/rust-influxdb-udp-logger dreame:/data/
# scp -O target/aarch64-unknown-linux-gnu/release/rust-influxdb-udp-logger dreame:/data/
# /data/rust-influxdb-udp-logger --hostname dreame --valetudo localhost:80 --protocol tcp --target 192.168.16.4:8089 >> /tmp/victoria_logger.log 2>&1 &
# 2024-08-16 without battery: 510304 -> 235692
# 2024-08-16 with    battery: 530784 -> 242228 
# 2024-08-16 without servers: 452960 -> 212340

#target=x86_64-unknown-linux-musl
#target=aarch64-unknown-linux-musl

## pi4 signal_notifier testing
#target=aarch64-unknown-linux-gnu
#features=thermal_zones,signal_notifier

## qnap
#target=x86_64-unknown-linux-musl
#features=sysinfo,thermal_zones,http_server,track_source,external,mqtt_relay
# 2024-07-18 10:20: 5166184 -> 1757448 = 5.17mb -> 1.76mb
# 2024-07-18 13:00: 5166184 -> 1756952 = 5.17mb -> 1.76mb
## that rumqttd lib is a huge beast...

## build with debugging info telling which src file and line caused a panic:
#debug=1

RUSTFLAGS="-Zlocation-detail=none"
z0="build-std=std,panic_unwind"
z1="build-std=std,panic_abort"
z2="build-std-features=panic_immediate_abort"
if [ "$target" == "x86_64-unknown-linux-gnu" ]; then
  bin=cargo
else
  bin=cross
fi
if [ $debug ]; then
  $bin +nightly build --target $target --release --no-default-features -F $features
else
  $bin +nightly build -Z $z1 -Z $z2 --target $target --release --no-default-features -F $features
fi



ls -al target/$target/release/rust-influxdb-udp-logger

upx --best --lzma target/$target/release/rust-influxdb-udp-logger

ls -al target/$target/release/rust-influxdb-udp-logger

# with http = reqwest + ssl:
# 2335256 -> 710184 = 2.3mb -> 710kb

# without http = reqwest + ssl:
# 1612704 -> 475112 = 1.6mb -> 475kb

# without http & external (=regex crate)
# 627224 -> 254316 = 627kb -> 254kb

# without http & external & fronius (= modbus & binread crates)
# 559608 -> 233660 = 560kb -> 234kb

# without http & external & fronius & valetudo (=serde_json crate)
# 506344 -> 207164 = 506kb -> 207kb

# without http & external & fronius & valetudo & battery & sysload
# 438864 -> 183380 = 439kb -> 183kb

# with only the feature "thermal_zones"
# 431488 -> 182664 = 431kb -> 183kb

# with only the feature "sysinfo" (uses crate with same name)
# 458632 -> 194100 = 459kb -> 194kb

# $ cross build --no-default-features -F battery,sysinfo,http_server --release --target x86_64-pc-windows-gnu
# $ ls -ali target/x86_64-pc-windows-gnu/release/rust-influxdb-udp-logger.exe 
# 114846 -rwxr-xr-x 2 user user 882176 Jul 10 19:21 target/x86_64-pc-windows-gnu/release/rust-influxdb-udp-logger.exe
# $ ls -alih target/x86_64-pc-windows-gnu/release/rust-influxdb-udp-logger.exe 
# 114846 -rwxr-xr-x 2 user user 862K Jul 10 19:21 target/x86_64-pc-windows-gnu/release/rust-influxdb-udp-logger.exe

# $ RUSTFLAGS="-Zlocation-detail=none" cross build --no-default-features -F battery,sysinfo,http_server --release --target x86_64-pc-windows-gnu
# $ ls -ali target/x86_64-pc-windows-gnu/release/rust-influxdb-udp-logger.exe 
# 352677 -rwxr-xr-x 2 user user 856576 Jul 10 19:29 target/x86_64-pc-windows-gnu/release/rust-influxdb-udp-logger.exe
# $ ls -alih target/x86_64-pc-windows-gnu/release/rust-influxdb-udp-logger.exe 
# 352677 -rwxr-xr-x 2 user user 837K Jul 10 19:29 target/x86_64-pc-windows-gnu/release/rust-influxdb-udp-logger.exe

# 2024-07-12 target=aarch64-unknown-linux-gnu & features=battery,sysinfo,thermal_zones,http_server,fronius,external
# 1345896 -> 412368 = 1346kb -> 412kb

# 2024-07-12 target=aarch64-unknown-linux-gnu & features=sysinfo
# 395616 -> 180640 = 396kb -> 180kb

# 2024-07-13 target=aarch64-unknown-linux-gnu & features=battery,sysinfo,thermal_zones,http_server,fronius,external - after external feature recovery: ability to return value with (parts of) prefix instead of just float-value:
# 1349992 -> 412256 = 1350kb -> 412kb

# 2024-07-14 target=aarch64-unknown-linux-gnu
# features=battery,sysinfo,thermal_zones,http_server,fronius,external,influxdb_relay,track_source
# 1366376 -> 416820 = 1366kb -> 417kb

# 2024-07-24 22:19
#target=aarch64-unknown-linux-gnu
#features=battery,sysinfo,thermal_zones,http_server,fronius,external,influxdb_relay,track_source,signal_notifier
#no panic debug info
# 1386856 -> 425700 = 1387kb -> 426kb

# 2024-07-26 19:18 # need to embedded ssl for worx = https -> ssl client, otherwise compiler error: could not find system library 'openssl' required by the 'openssl-sys' crate
#target=aarch64-unknown-linux-gnu
#features=battery,sysinfo,thermal_zones,http_server,fronius,external,influxdb_relay,track_source,signal_notifier,worx
# 6239288 -> 236760 = 6.2mb -> 2.4mb